__title__ = 'Tuning Conduction Velocity'
__description__ = 'This tutorial introduces the background for the relationship between conduction velocity and tissue conductivity'
__image__ = '/images/02_03A_tuneCV-Setup1D.png'