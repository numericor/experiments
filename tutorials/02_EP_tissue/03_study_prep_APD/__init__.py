__title__ = 'APD adjustment'
__description__ = 'This example demonstrates how to adjust ionic model parameters to generate a specific action potential duration in your simulations'