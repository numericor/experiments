__title__ = 'Simple example to base your own experiment on'
__description__ = 'This is a simple example covering mesh generation, monodomain simulation and local activation time extraction during postprocessing. It can be a good starting point to base your own experiment on.'
__image__ = '/images/02_22_code.png'