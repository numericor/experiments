#!/usr/bin/env python

"""
This is a simple monodomain example that is primarily a demonstration of the
carputils framework. It includes all typical building blocks of a cardiac
electrophysiology experiment starting from mesh generation / loading, stimulus definition,
conductivity and ionic model definition and launching the simulation.
Finally, local activation times are determined as a postprocessing step and the
results are visualized using meshalzyer.
As such, this script can be a good starting point to build your own experiment
using carputils.

This webpage only shows the basic documentation, to use it as a starting point for 
building your own experiment, you should look at the `Python code
<https://git.opencarp.org/openCARP/experiments/-/blob/master/tutorials/02_EP_tissue/22_simple/run.py>`__ of this
experiment.

For detailed information on the different functions and parameters, consult
the `carputils documentation <https://opencarp.org/documentation/carputils-documentation>`__.


Problem Setup
=============

This example defines a small cuboid on the domain:

.. math::

    -1.0  \leq x \leq 1.0

    -0.25 \leq y \leq 0.25

    -0.25 \leq z \leq 0.25

And applies an electrical stimulus on the :math:`x = -1.0` face. 


Usage
=====

This example specifies just one optional argument, ``--tend``. Use it to
specify how long to simulate for after stimulus, in milliseconds:

.. code-block:: bash

    ./run.py --tend 100

As with other examples, add the ``--visualize`` option to automatically load
the results in meshalyzer:

.. code-block:: bash

    ./run.py --tend 100 --visualize
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Simple carputils Example'
EXAMPLE_AUTHOR = 'Andrew Crozier <andrew.crozier@medunigraz.at>, Axel Loewe <axel.loewe@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

# import required carputils modules
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

# define parameters exposed to the user on the commandline
def parser():
    parser = tools.standard_parser()
    parser.add_argument('--tend',
                        type=float, default=20.,
                        help='Duration of simulation (ms). Run for longer to also see repolarization.')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_simple_{}_{}_np{}'.format(today.isoformat(), args.tend,
                                         args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh, units in mm, centered around origin
    # By default, elements are tagged as "1". Can be changed by adding box or sphere regions
    # See carputils doxygen documentation for details
    geom = mesh.Block(size=(2, 0.5, 0.5))

    # Set fibre angle to 0°, sheet angle to 90° both on "endo" and "epi" surfaces
    geom.set_fibres(0, 0, 90, 90)
    
    # Generate and return base name
    meshname = mesh.generate(geom)

    # Instead of generating a mesh, you could also load one using the 'meshname' openCARP
    # command. This requires .elem and .pts files that can be generated for VTK files using
    # meshtool 
    
    # Get basic command line, including solver options from external .par file
    #cmd = tools.carp_cmd(tools.carp_path(os.path.join(EXAMPLE_DIR, 'simple.par')))
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'simple.par'))
    
    # Attach electrophysiology physics (monodomain) to mesh region with tag 1
    cmd += tools.gen_physics_opts(IntraTags=[1])

    cmd += ['-simID',    job.ID,
            '-meshname', meshname,
            '-tend',     args.tend]

    # Set monodomain conductivities
    cmd += ['-num_gregions',			1,
    		'-gregion[0].name', 		"myocardium",
    		'-gregion[0].num_IDs', 	1,  		# one tag will be given in the next line
    		'-gregion[0].ID', 		"1",		# use these setting for the mesh region with tag 1
    		# mondomain conductivites will be calculated as half of the harmonic mean of intracellular
    		# and extracellular conductivities
    		'-gregion[0].g_el', 		0.625,	# extracellular conductivity in longitudinal direction
    		'-gregion[0].g_et', 		0.236,	# extracellular conductivity in transverse direction
    		'-gregion[0].g_en', 		0.236,	# extracellular conductivity in sheet direction
    		'-gregion[0].g_il', 		0.174,	# intracellular conductivity in longitudinal direction
    		'-gregion[0].g_it', 		0.019,	# intracellular conductivity in transverse direction
    		'-gregion[0].g_in', 		0.019,	# intracellular conductivity in sheet direction
    		'-gregion[0].g_mult',		0.5		# scale all conducitivites by a factor (to reduce conduction velocity)    		
    		]

    # compute local activation times in postprocessing
    cmd += ['-num_LATs',           1,
           '-lats[0].ID',         'activation',
           '-lats[0].all',        0,	# only detect first activation
           '-lats[0].measurand',  0,	# determine LAT from transmembrane voltage
           '-lats[0].mode',       0,	# take maximum slope to detemine LAT
           '-lats[0].threshold', -10,	# -10mV threshold
           ]

	# Define the geometry of the first stimulus (index 0) at end of the block
    # (upper bound of x coordinate), units in um
    stim = mesh.block_bc_opencarp(geom, 'stim', 0, 'x', True)
    cmd += stim

    if args.visualize:
        cmd += ['-gridout_i', 3]	# output both surface & volumetric mesh for visualization

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        # display trensmembrane voltage
        data = os.path.join(job.ID, 'vm.igb.gz')
        # Alternatively, you could visualize the activation times instead
        # data = os.path.join(job.ID, 'init_acts_activation-thresh.dat')
        
        # load predefined visualization settings
        view = os.path.join(EXAMPLE_DIR, 'simple.mshz')

        # Call meshalyzer
        job.meshalyzer(geom, data, view)

if __name__ == '__main__':
    run()
