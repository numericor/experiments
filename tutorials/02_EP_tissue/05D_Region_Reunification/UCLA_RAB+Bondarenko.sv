-87.7728            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.000109186         # Iion
-                   # tension_component
UCLA_RAB
174.83              # Ca_i
0.602434            # K_i
13.2633             # Na_i
1.68831e-05         # c1
0.976468            # c2
107.898             # cj
107.803             # cjp
2.98318             # cp
0.206227            # cs
0.0125              # gKr
0.173558            # gKs
0.991778            # h
0.994527            # j
0.000980194         # m
15.9121             # tropi
17.9661             # trops
3.11406e-05         # xi1ba
0.00194481          # xi1ca
0.0144178           # xi2ba
0.00712013          # xi2ca
0.0151172           # xir
0.0065007           # xr
0.00534789          # xs1
0.0242719           # xs2
0.00351158          # xtof
0.00351186          # xtos
0.995483            # ytof
0.370594            # ytos
0                   # __acap_local
0                   # __fr_myo_local
0                   # __sl_i2c_local
0                   # __vcell_local

INa_Bond_Markov
0.000106259         # C_Na1
0.0145355           # C_Na2
3.08381e-05         # I1_Na
0.00150861          # I2_Na
0.00394685          # IC_Na2
0.20924             # IC_Na3
2.88979e-05         # IF_Na
1.34697e-07         # O_Na

