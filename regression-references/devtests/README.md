# CARP Developer Test Reference Solutions

This git repository contains reference solutions for the 
[openCARP developer tests](
https://git.opencarp.org/openCARP/experiments/-/tree/master/regression/devtests).

