
#!/usr/bin/env python

"""
This example provides pure mechanics and electromechanics examples on a simple
ring geometry.

Problem Setup
=============

This problem generates a simple ring mesh using the
:class:`carputils.mesh.Ring` class. The ring is tesselated into tetrahedra as
shown below:

.. image:: /images/ring_mesh.png
    :scale: 70%

In all experiment types in this example, the top and bottom surfaces of the
ring are constrained to lie in the same plane with Dirichlet boundary
conditions, and an additional three nodes on the bottom (:math:`z=0`) surface
are constrained such that free body rotation and translation is prevented.
Two nodes on the x axis are prevented from moving in the y direction, and one
node on the y axis is prevented from moving in the x direction:

.. image:: /images/ring_rigid_bc.png

Experiments
===========

Several experiments are defined:

* ``active-free`` - Run an active contraction simulation without constraints on
  cavity size or pressure
* ``active-iso`` - Run an active contraction simulation with an isovolumetric
  cavity constraint
* ``active-pv-loop`` - Run an active contraction stimulation with pressure/flux
  constraints imposed by Windkessel or circulatory models coupled to both
  RV and LV cavity. In this case both left and right ventricular cavities are
  coupled to a 3-element Windkessel model
"""

import os
import math
from datetime import date

from carputils import settings
from carputils import tools
from carputils import ep
from carputils import model
from carputils.mesh import Ring, linear_fibre_rule
from carputils import testing
from carputils.resources import trace
from carputils.resources import petsc_options
from carputils.resources import petsc_block_options
from devtests.mechanics import standardmesh

EXAMPLE_DESCRIPTIVE_NAME = 'Ring'
EXAMPLE_AUTHOR = ('Gernot Plank <gernot.plank@medunigraz.at>,'
                  'Christoph Augustin <christoph.augustin@medunigraz.at> and'
                  'Matthias Gsell <matthias.gsell@medunigraz.at>')

# =============================================================================
#    Command-line PARSER
# =============================================================================
def parser_commands():
    """ Setup command line parser. """
    parser = tools.standard_parser()
    standardmesh.parser_add_materialopts(parser, default='usyk')
    group = parser.add_argument_group("experiment specific options")
    group.add_argument('--experiment',
                       default='active-free',
                       choices=['active-free',
                                'active-iso',
                                'active-inflate',
                                'active-pv-trace',
                                'active-pv-loop'],
                       help='pick experiment type (default: active-free)')
    group.add_argument('--propagation',
                       default='R-E+',
                       choices=['R-D', 'R-E+', 'R-E-'],
                       help='pick propagation driver, either R-D or R-E+')
    group.add_argument('--duration',
                       default=0.0, type=float,
                       help='duration of experiment (default: 0 ms value is \
                             computed by PCL and number of beats)')
    group.add_argument('--stimulus',
                       default='endocardium',
                       choices=['none', 'endocardium'],
                       help='pick stimulus procedure (default: endocardium)')
    group.add_argument('--PCL',
                       type=float, default=1000.,
                       help='Pacing cycle length')
    group.add_argument('--beats',
                       type=int, default=1,
                       help='Number of pacing pulse')
    group.add_argument('--mechanics_off',
                       action='store_true',
                       help='switch off mechanics to generate activation vec')
    group.add_argument('--fast',
                       action='store_true',
                       help='Run for speed (coarser mesh, simple material)')
    group.add_argument('--pstress_off',
                       action='store_true',
                       help='switch off passive stress calculation')
    group.add_argument('--cvs-model',
                       default='wk3',
                       choices=['wk3', 'circadapt', 'nektar1D'],
                       help='cardiovascular system model\
                            ("circadapt" and "nektar1D" are experimental)')
    group.add_argument('--preload',
                       type=float, default=8,
                       help='LA pressure used to drive filling')
    group.add_argument('--stress', default='landhuman',
                       choices=list(model.activetension.keys()),
                       help='pick model for active stress')
    group.add_argument('--s_peak',
                       type=float, default=50.0,
                       help='Peak stress in kPa')
    group.add_argument('--cavity',
                       default='LV',
                       choices=['LV', 'LA', 'RV', 'RA'],
                       help='Choose cavity')
    group.add_argument('--ldOn', default=False, action='store_true',
                       help='Turn on lenght dependence if supported')
    group.add_argument('--hexahedra',
                       type=str, default='off', choices=['on', 'off'],
                       help='Switch to hexahedral elements.')
    return parser

# --- setup job ID -------------------------------------------------------------
def setup_job_id(args):
    """ Generate name of top level output directory. """
    today = date.today()
    subtest = args.experiment
    if args.fast:
        subtest += '_fast'
    tpl = '{}_{}_{}_{}_np{}'
    if args.mech_with_inertia:
        tpl = '{}_{}_{}_{}_GA_np{}'

    return tpl.format(today.isoformat(), subtest, args.mech_element,
                      args.flv, args.np)

# =============================================================================
#    MAIN
# =============================================================================
@tools.carpexample(parser_commands, setup_job_id)
def run(args, job):
    """ MAIN """

    mesh_name = generate_mesh(args)

    cmd = tools.carp_cmd()
    cmd += tools.gen_physics_opts(MechTags=[1], IntraTags=[1])

    cmd += ['-simID', job.ID,
            '-meshname', mesh_name,
            '-output_level', 10]

    cmd += setup_timers(args)

    cmd += setup_solver(args)

    cmd += setup_ep(args, mesh_name)

    cmd += setup_mech(args, mesh_name)

    cmd += setup_cardiovascular_system(args, mesh_name)

    cmd += setup_visualization(args)

    # Run main CARP simulation
    job.carp(cmd)

    # Visualize
    if args.visualize and not settings.platform.BATCH and not args.dry:
        visualize_meshalyzer(job, args, mesh_name)

# =============================================================================
#    General functions
# =============================================================================

def generate_mesh(args):
    """ Generate mesh
    Args:
        args: parser arguments
    Returns:
        mesh name
    """
    if args.hexahedra == 'on':
        suffix = 'hex'
    else:
        suffix = 'tet'

    mesh_dir = 'mesh'
    mesh_name = f'{mesh_dir}/ring_{suffix}'

    if args.postprocess:
        return  mesh_name  # mesh already created

    if not os.path.exists(mesh_dir):
        os.mkdir(mesh_dir)

    if args.cvs_model in ['circadapt', 'nektar1D'] or not args.fast:
        lv_rad = 50.0
    else:
        lv_rad = 25.0

    if args.fast:
        p60m60 = linear_fibre_rule(60, -60)
        mesh = Ring(lv_rad, height=10.0, div_transmural=1, div_height=1,
                    div_circum=50, fibre_rule=p60m60,
                    tetrahedrise=(args.hexahedra == 'off'))
    else:
        p60m60 = linear_fibre_rule(60, -60)
        mesh = Ring(lv_rad, height=20.0, div_transmural=3, div_height=2,
                    fibre_rule=p60m60,
                    tetrahedrise=(args.hexahedra == 'off'))

    mesh.generate_carp(mesh_name, ['top', 'bottom', 'inside', 'outside'])
    mesh.generate_carp_rigid_dbc(mesh_name)

    return mesh_name

# -----------------------------------------------------------------------------
def setup_timers(args):
    """ Sets the timers for the electro-mechanical simulation
    Args:
        args: parser arguments
    Returns:
        List of timer options.
    """

    if args.mechanics_off: # turn off mechanics
        mech_dt = 0.0
    else:
        mech_dt = 1.

    ep_dt = min(float(args.PCL)*1000, 10.)
    time_dt = min(float(args.PCL), 5.)
    space_dt = min(float(args.PCL), 1.)
    if args.duration > 0.0:
        duration = args.duration
    else:
        duration = args.PCL*args.beats

    if args.postprocess:
        space_dt = 5

    # cvs clock
    opts = setup_cvsys_clock(args)

    # TODO set up trigger probes
    #opts += setup_trigger_probes()

    opts += ['-dt', ep_dt,          # timestep for electrics [us]
             '-tend', duration,     # length of the simulation [ms]
             '-timedt', time_dt,    # time between temporal output [ms]
             '-spacedt', space_dt,  # time between spacial output [ms]
             '-mechDT', mech_dt]    # timestep for mechanics [ms]

    return opts

# -----------------------------------------------------------------------------
def setup_solver(args):
    """ Setup solver options for the simulation
    Returns:
        List of solver options
    """
    lopts = ['-mech_krylov_maxit', 200]
    # change default solver options
    if args.flv == 'petsc':
        if args.mech_element == 'P1-P0':
            lopts += ['-mech_options_file', petsc_options.path('gamg_gmres_opts_agg')]
        else:
            lopts += ['-mech_options_file', petsc_block_options.path('fgmres_fieldsplit_mech_petsc_agg')]


    # Newton options
    maxit = 3
    if args.cvs_model == 'wk3':  # wk3
        maxit = 15

    init_beats = args.beats - 1
    nopts = ['-mech_newton_options.maxit', maxit,
             #'-IO_num_init_beats', init_beats,
             '-mech_newton_options.num_init_beats[0]', 2,
             '-mech_newton_options.num_init_beats[1]', init_beats,
             '-mech_newton_options.use_adaptive_tol', 0]

    return lopts + nopts

# =============================================================================
#    EP FUNCTIONS
# =============================================================================
# -----------------------------------------------------------------------------
def setup_ep(args, mesh_name):
    """ setup electrophysiology
    Args:
        args: parser arguments
        mesh_name: mesh name
    Returns:
        List of EP options
    """

    # conductivity region setup
    g_settings = ['-num_gregions', 1,         # number of conductivity regions
                  '-gregion[0].name', 'active_ring',
                  '-gregion[0].num_IDs', 1,
                  '-gregion[0].ID[0]', 1,
                  '-gregion[0].g_il', 0.174,  #intracellular conductivity along the fibers
                  '-gregion[0].g_it', 0.019,  #intracellular conductivity transverse to the fibers
                  '-gregion[0].g_in', 0.019,  #intracellular conductivity in sheet normal direction
                  '-gregion[0].g_el', 0.625,  #extracellular conductivity along the fibers
                  '-gregion[0].g_et', 0.236,  #extracellular conductivity transverse to the fibers
                  '-gregion[0].g_en', 0.236]  #extracellular conductivity in sheet normal direction

    # stimuli
    stimdur = 2.0
    num_stims, stim_options = setup_stimuli(args, mesh_name, stimdur)

    # propagation in R-E mode
    num_ekregions = 1
    ek_settings = ['-num_ekregions', num_ekregions,
                   '-ekregion[0].ID', 0,
                   '-ekregion[0].name', 'ek_ring',
                   '-ekregion[0].vel_n', 0.6,
                   '-ekregion[0].vel_s', 0.4,
                   '-ekregion[0].vel_f', 0.2]

    ek_stim, num_stims = ep.fake_ep_opts(num_stims, '', None, num_pulses=args.beats,
                                         bcl=args.PCL, stim_duration=stimdur)
    # TODO
    # ek_stim += [f'-stimulus[{num_stims-1}].xtrg', 6]
    stim_options += ek_stim

    ep_options = g_settings + ek_settings
    ep_options += ['-num_stim', num_stims] + stim_options

    return ep_options

# -----------------------------------------------------------------------------
def setup_cvsys_clock(args):
    """ cvsys clock """
    cvs_clock = ['-fcclock.SA_CL', args.PCL,
                 '-fcclock.ERP', args.PCL*0.9,
                 '-fcclock.SA_t0', 0.0,
                 '-fcclock.AV_delay', args.PCL*0.1,
                 '-fcclock.VV_delay', 0.0,
                 '-fcclock.AA_delay', args.PCL*0.02]

    return cvs_clock

# -----------------------------------------------------------------------------
def setup_trigger_probes():
    """ clock-derived triggers """
    # SA node
    sa_trg = ['-trigger[0].src', 0,
              '-trigger[0].clock_ID', 0]
    # RA
    ra_trg = ['-trigger[1].src', 0,
              '-trigger[1].clock_ID', 1]
    # LA
    la_trg = ['-trigger[2].src', 0,
              '-trigger[2].clock_ID', 2]
    # AVa, atrial entry site of AV node
    ava_trg = ['-trigger[3].src', 0,
               '-trigger[3].clock_ID', 3]
    # AVv, ventricular exit site of AV node
    avv_trg = ['-trigger[4].src', 0,
               '-trigger[4].clock_ID', 4]
    # RV
    rv_trg = ['-trigger[5].src', 0,
              '-trigger[5].clock_ID', 5]
    # LV
    lv_trg = ['-trigger[6].src', 0,
              '-trigger[6].clock_ID', 6]
    trigs = sa_trg + ra_trg + la_trg + ava_trg + avv_trg + rv_trg + lv_trg
    return ['-num_trgs', 7] + trigs

# --- setup stimulus ----------------------------------------------------------
def setup_stimuli(args, mesh_name, stimdur):
    """ setup stimulus
    Args:
        stimulus: stimulus procedure
        mesh_name: mesh name
    """

    # Generate electrical trigger options
    stim_opts = []
    num_stims = 0                           # initialize number of stimuli
    stim_strength = 80.                     # stimulus strength in mA/cm^2
    stim_duration = min(args.PCL, stimdur)  # stimulus duration in ms

    # define stimulus
    if args.stimulus in ['endocardium', 'none']:

        bc_tpl = mesh_name + '_{}'
        lv_endo = bc_tpl.format('inside')

        stim_opts = ['-stim[0].elec.vtx_file', lv_endo,
                     '-stim[0].crct.type', 0,
                     '-stim[0].pulse.strength', stim_strength,
                     '-stim[0].ptcl.start', 0,
                     '-stim[0].ptcl.duration', stim_duration]

        # stimulation controlled by LV cvsys clock
        xtrg = 0   # right atrium
        if args.cavity == 'LV':
            xtrg = 6   # left ventricle
        elif args.cavity == 'RV':
            xtrg = 5   # right ventricle
        elif args.cavity == 'LA':
            xtrg = 2   # left atrium
        elif args.cavity == 'RA':
            xtrg = 1   # right atrium
        # TODO
        # stim_opts += ['-stimulus[0].xtrg', xtrg,
        #               '-stimulus[0].xtrg_offset', 0.0]

        num_stims += 1

    else:
        raise Exception('Unknown stimulus option.')

    return num_stims, stim_opts

# =============================================================================
#    Mechanics FUNCTIONS
# =============================================================================
# -----------------------------------------------------------------------------
def setup_mech(args, mesh_name):
    """ setup generic mechanical settings, materials
    Args:
        args: parser arguments
        mesh_name: mesh name
    Returns:
        List of mechanics options
    """

    # Set variational formulation options
    kappa = 650 if args.mech_element == 'P1-P0' else 1e100
    if args.parameters == []:
        args.parameters.append(('kappa', kappa))

    # Set passive material properties
    mat = standardmesh.material_from_parser(args)
    pass_mat = model.optionlist([mat]) # Material definitions

    # Set active material parameters
    ep_model = model.ionic.get('tt2')
    ep_params = {}
    at_model = model.activetension.get(args.stress)
    # overrule default parametrization?
    if args.stress in ['tanh', 'converted_tanhstress']:
        l_d = 1 if args.ldOn else 0
        args.stress = 'tanh'
        ta_params = update_tanh_stress_pars(args.s_peak, 25.0, l_d)
    elif args.stress == 'landhuman':
        ta_params = update_land_human_stress_pars(args.s_peak)
    elif args.stress == 'land':
        ta_params = update_land_stress_pars(args.s_peak)
    else:
        ta_params = {}

    state_f = f'./states/tt2_{args.stress}_bcl_850_ms.sv'
    if os.path.exists(state_f) and args.stress in ['land', 'landhuman']:
        ep_params['im_sv_init'] = state_f  # add state file
    active = ep_model([0], 'active_ring', **ep_params)
    active.set_stress(at_model(**ta_params))

    act_mat = model.optionlist([active])

    mech_opts = ['-mech_tangent_comp', 0]   # 0 exact, 1 approximate

    mech_opts += ['-active_stress_mode', 5,
                  '-mech_lambda_upd', 2]

    mech_opts += pass_mat + act_mat

    if args.experiment != 'active-free':
        mech_opts += ['-mech_load_steps', 20]

    # Dirichlet Boundary conditions
    mech_opts += setup_dbc(mesh_name)

    # Neumann boundary conditions
    mech_opts += setup_nbc(args, mesh_name)

    return mech_opts

# -----------------------------------------------------------------------------
def setup_dbc(mesh_name):
    """ setup Dirichlet boundary conditions

    Fix geometry or prescribe a given displacement on part of the boundary
    Gamma_D > 0
    Dirichlet BC are essential for the existence of a solution.

    Args:
        mesh_name: mesh name

    Returns:
        List of Dirichlet BC options
    """

    bc_tpl = mesh_name + '_{}'

    # Dirichlet Boundary Conditions
    dbc_idx = 0

    # Fix bottom to xy plane
    dbc_opts = [f'-mechanic_dbc[{dbc_idx}].name', 'bottom_plane',
                f'-mechanic_dbc[{dbc_idx}].bctype', 0,
                f'-mechanic_dbc[{dbc_idx}].vtx_file', bc_tpl.format('bottom'),
                f'-mechanic_dbc[{dbc_idx}].apply_ux', 0,
                f'-mechanic_dbc[{dbc_idx}].apply_uy', 0,
                f'-mechanic_dbc[{dbc_idx}].apply_uz', 1]
    dbc_idx += 1

    ## Fix top to xy plane
    dbc_opts += [f'-mechanic_dbc[{dbc_idx}].name', 'top_plane',
                 f'-mechanic_dbc[{dbc_idx}].bctype', 0,
                 f'-mechanic_dbc[{dbc_idx}].vtx_file', bc_tpl.format('top'),
                 f'-mechanic_dbc[{dbc_idx}].apply_ux', 0,
                 f'-mechanic_dbc[{dbc_idx}].apply_uy', 0,
                 f'-mechanic_dbc[{dbc_idx}].apply_uz', 1]
    dbc_idx += 1


    # Prevent free rotation and translation
    dbc_opts += [f'-mechanic_dbc[{dbc_idx}].name', 'xaxis_fix',
                 f'-mechanic_dbc[{dbc_idx}].bctype', 0,
                 f'-mechanic_dbc[{dbc_idx}].vtx_file', bc_tpl.format('xaxis'),
                 f'-mechanic_dbc[{dbc_idx}].apply_ux', 0,
                 f'-mechanic_dbc[{dbc_idx}].apply_uy', 1,
                 f'-mechanic_dbc[{dbc_idx}].apply_uz', 1]
    dbc_idx += 1

    dbc_opts += [f'-mechanic_dbc[{dbc_idx}].name', 'yaxis_fix',
                 f'-mechanic_dbc[{dbc_idx}].bctype', 0,
                 f'-mechanic_dbc[{dbc_idx}].vtx_file', bc_tpl.format('yaxis'),
                 f'-mechanic_dbc[{dbc_idx}].apply_ux', 1,
                 f'-mechanic_dbc[{dbc_idx}].apply_uy', 0,
                 f'-mechanic_dbc[{dbc_idx}].apply_uz', 1]
    dbc_idx += 1

    return ['-num_mechanic_dbc', dbc_idx] + dbc_opts

# -----------------------------------------------------------------------------
def setup_nbc(args, mesh_name):
    """ setup Neumann boundary conditions

    Prescribe pressure or traction to part of the boundary Gamma_N

    Args:
        mesh_name: mesh name

    Returns:
        List of Neumann BC options
    """

    bc_tpl = mesh_name + '_{}'
    pressure = 0.0                         # pressure in kPa
    nb_trc = trace.path('constant')       # prescribed pressure trace
    if args.experiment == 'active-inflate':
        pressure = args.preload*0.133322
    elif args.experiment == 'active-pv-trace':
        pressure = args.preload*0.133322
        nb_trc = trace.path('pressureLAP')  # prescribed pressure trace

    # Neumann Boundary Conditions
    nbc_idx = 0
    bs_idx = 0
    nbc_opts = [f'-mechanic_nbc[{nbc_idx}].name', 'lv_endo',
                f'-mechanic_nbc[{nbc_idx}].pressure', pressure,
                f'-mechanic_nbc[{nbc_idx}].surf_file', bc_tpl.format('inside'),
                f'-mechanic_nbc[{nbc_idx}].trace', nb_trc]
    nbc_idx += 1

    nbc_opts += [f'-mechanic_nbc[{nbc_idx}].name', 'top',
                 f'-mechanic_nbc[{nbc_idx}].surf_file', bc_tpl.format('top'),
                 f'-mechanic_nbc[{nbc_idx}].spring_idx', bs_idx,
                 f'-mechanic_bs[{bs_idx}].value', 1e-5,
                 f'-mechanic_bs[{bs_idx}].edidx', -1]
    bs_idx += 1
    nbc_idx += 1
    nbc_opts += [f'-mechanic_nbc[{nbc_idx}].name', 'bottom',
                 f'-mechanic_nbc[{nbc_idx}].surf_file', bc_tpl.format('bottom'),
                 f'-mechanic_nbc[{nbc_idx}].spring_idx', bs_idx,
                 f'-mechanic_bs[{bs_idx}].value', 1e-5,
                 f'-mechanic_bs[{bs_idx}].edidx', -1]
    bs_idx += 1
    nbc_idx += 1

    return ['-num_mechanic_nbc', nbc_idx] + ['-num_mechanic_bs', bs_idx] + nbc_opts

# =============================================================================
#    Active stress functions
# =============================================================================

# --- Tanh stress parameters --------------------------------------------------
def update_tanh_stress_pars(s_peak, tau_c, length_dep):
    """ Set parameters for the Tanh active stress model

    see Niederer et al 2011 Cardiovascular Research 89
    and the thesis of Andrew Crozier
    not mentioned parameters will have the default value

    Length dependence is regulated with 'ld' and 'ld_on'
    larger ld means steeper dependence,
    depending on lambda more or less heterogeneity

    Args:
        s_peak: peak magnitude of stress transient
        tau_c: time constant contraction

    Returns:
        parameter settings for the Tanh active stress model
    """
    param = {'Tpeak': s_peak,
             'tau_c0': 20,
             'tau_r': 20.0,
             'ld': 10.0,
             't_dur': 110.0,
             'lambda_0': 0.6,
             'ldOn': length_dep}
    return param

# -----------------------------------------------------------------------------
def update_land_stress_pars(s_peak):
    """ Set parameters for the Land et al. active stress model
    see Land et al. 2012 J Physiol 590.18
    not mentioned parameters will have the default value

    Args:
        s_peak: peak magnitude of stress transient

    Returns:
        parameter settings for the Land et al. active stress model
    """
    param = {'T_ref': s_peak,
             'Ca_50ref': 0.12,
             'TRPN_50': 0.37,
             'n_TRPN': 0.54,
             'k_TRPN': 0.02,
             'n_xb': 2.0,
             'k_xb': 0.02}
    return param

# -----------------------------------------------------------------------------
def update_land_human_stress_pars(s_peak):
    """
    Land stress parameters
    Active stress model as used in Land et al. 2012 J Physiol 590.18
    not mentioned parameters will have the default value
    """
    # current settings      #   default values (see LandHumanStress.model in LIMPET)
    # --------------------- # -------------------------------------------------
    param = {               #   direct (dp) and indirect proportional (ip)
        'Tref': s_peak,     #   Tref    = 120   (T_ref)  dp with Sa_amp
        'TRPN_n': 2.0,      #   TRPN_n  = 2.0   (n_TRPN) ip with relax time (SaTR*)
        'ca50': 0.805,      #   ca50    = 0.805 (ca_ref_t50) ip with relax time (SaTR*)
        'perm50': 0.30,     #   perm50  = 0.35  (TRPN_50) ip with rise time (SATP*)
        'nperm': 8.0,       #   nperm   = 5.0   (n_Tm) dp with rise time (SATP*)
        'dr': 0.25}         #   dr      = 0.25  (r_s) ip with Sa_amp
    return param

# =============================================================================
#    Cardiovascular system functions
# =============================================================================

def setup_cardiovascular_system(args, mesh_name):
    """ setup cardiovascular system

    Args:
        args: parser arguments
        mesh_name: mesh name

    Returns:
        A list of cardiovascular system options cv_opts.
    """

    # setup cavity volumes
    num_cavs = 1
    cv_opts = setup_cavity_volumes(mesh_name, num_cavs)

    # in the active-free case we don't apply any pressure in the cavity
    # but we want to keep track of the cavity volume
    if args.experiment in ['active-free']:
        return cv_opts

    # link volume and pressure boundaries to define cavity
    cv_opts += cv_sys_parameters(args, num_cavs)

    return cv_opts

# --- Setup cavity volume computation -----------------------------------------
def setup_cavity_volumes(mesh_name, num_cavs):
    """ setup computation of cavity volumes
    Args:
        mesh_name: mesh name
        num_cavs: number of cavities

    Returns:
        A list of cavity volume options.
    """

    bc_tpl = mesh_name + '_{}'
    surf_file = bc_tpl.format('inside') # surface file enclosing the volume

    grid_id = model.mechanics.grid_id()

    cav_vol_opts = ['-numSurfVols', num_cavs,
                    '-surfVols[0].name', 'lv_endo',
                    '-surfVols[0].surf_file', surf_file,
                    '-surfVols[0].grid', grid_id,
                    '-surfVols[0].vecf_x', 1.0,
                    '-surfVols[0].vecf_y', 0.0,
                    '-surfVols[0].vecf_z', 0.0]

    # element volumes
    vol_tracking = 1  # 1: track myocardial volume changes due to mech deform
    num_elem_vols = 1
    elem_vol_opts = ['-numElemVols', num_elem_vols,
                     '-elemVols[0].name', 'ring',
                     '-elemVols[0].numtags', 0,
                     '-elemVols[0].grid', grid_id,
                     '-volumeTracking', vol_tracking]

    return cav_vol_opts + elem_vol_opts

# --- Set cavity volume computation -----------------------------------------
def cv_sys_parameters(args, num_cavs):
    """ Set cardiovascular system parameters
    Returns:
        A list of the cardiovascular system parameters.
    """

    # set defaults for active-iso
    cav_state = 0  # 0: isovolumetric, valves closed
    cv_param = []  # default attached cvsys options
    p0_in = args.preload  # initial pressure
    cav_type = 0   # left ventricle
    p0_out = 80.0

    if args.experiment == 'active-pv-loop':
        cav_state = -1   # -1: state of cavity managed automatically

        if args.cavity == 'RV':
            cav_type = 1   # right ventricle
            p0_out = 10.0
        elif args.cavity == 'LA':
            cav_type = 2   # left atrium
            p0_out = 3.0
        elif args.cavity == 'RA':
            cav_type = 3   # right atrium
            p0_out = 3.0

        if args.cvs_model == 'wk3':  # wk3
            cvs_mode = 0  # standard coupling
            cv_param += setup_windkessel_parameters(args)

        if args.cvs_model == 'circadapt':
            cvs_mode = 1  # circAdapt based coupling
            cv_param += setup_circadapt_parameters(args)
        elif args.cvs_model == 'nektar1D':
            cvs_mode = 2  # circAdapt based coupling

        cv_param += ['-CV_coupling', 0,  #  0: coupling using cavitary volumes
                     '-CV_FE_coupling', 0,
                     '-CVS_mode', cvs_mode]

    elif args.experiment in ['active-inflate', 'active-pv-trace']:
        cav_state = 5  # -1: state of cavity managed automatically
        p0_out = p0_in
    else:  # active-iso
        p0_out = 0

    cv_param += ['-num_cavities', num_cavs,          # number of cavities
                 '-cavities[0].cav_type', cav_type,  # 0 LV 1 RV 2 LA 3 RA
                 '-cavities[0].cavP', 0,             # link to nbc
                 '-cavities[0].cavVol', 0,           # link to volume
                 '-cavities[0].p0_cav', p0_in,      # define initial pressure
                 '-cavities[0].p0_out', p0_out,      # init. p in outflow tube
                 '-cavities[0].p0_in', p0_in,        # init. p in inflow cav
                 '-cavities[0].state', cav_state]    # state management

    return cv_param

# ---------------------------------------------------------------------------
def setup_circadapt_parameters(args):
    """ setup circadapt parameters """
    lv_vol = 129      # [ml]
    rv_vol = lv_vol   # [ml]
    lv_wall_vol, lv_surf_area = cav_metrics(lv_vol, 10.0)  # wall thickness [mm

    rv_wall_vol, rv_surf_area = cav_metrics(rv_vol, 4.0)   # wall thickness [mm

    sv_wall_vol = (lv_wall_vol/100)*41.0

    prm = ['-cvs_mass_conservation', 1,
           '-cvs_allow_negative_p', 1,
           '-cvs_pulmonary_pressure_drop', 10,
           '-cvs_systemic_pressure_drop', 90,
           '-cvs_lv_vol', lv_vol,
           '-cvs_lv_wall_vol', lv_wall_vol,
           '-cvs_lv_surf_area', lv_surf_area,
           '-cvs_rv_vol', rv_vol,
           '-cvs_rv_wall_vol', rv_wall_vol,
           '-cvs_rv_surf_area', rv_surf_area,
           '-cvs_sv_wall_vol', sv_wall_vol,
           '-cvs_lv_actmax', 84000,
           '-cvs_lv_pasrest', 50000*0.5,
           '-cvs_rv_actmax', 84000,
           '-cvs_rv_pasrest', 50000*0.5,
           '-cvs_systemic_resistance_factor', 1.0,
           '-cvs_pulmonary_resistance_factor', 1.0,
           '-cvs_pulmonary_pressure_drop', 10.0,
           '-cvs_systemic_pressure_drop', 90.0,
           '-cvs_cycle_length', args.PCL*0.001,
           '-cvs_mitral_vlv_area', 750.,
           '-cvs_sarc_r_t_rise', 0.25,
           '-cvs_sarc_r_t_decay', 0.25]
           #'-cvs_stopping_crit', 2,
           #'-cvs_pv_diff_tol', 0.1]

    return prm

# ---------------------------------------------------------------------------
def cav_metrics(cav_vol, wall_thickness):
    """ compute cavity wall volume and surface area out of volume """
    cav_vol_m = cav_vol*1e-6  # [m]
    wall_thickness_m = wall_thickness*1e-3  #[m]
    cav_rad = ((3.0*cav_vol_m)/(4.0*math.pi))**(1.0/3.0)
    rad = cav_rad + wall_thickness_m
    wall_volume = (4.0/3.0)*math.pi*(rad*rad*rad-cav_rad*cav_rad*cav_rad)

    volm = cav_vol_m + 0.5*wall_volume
    rad = ((3.0*volm)/(4.0*math.pi))**(1.0/3.0)
    surf_area = 4.0*math.pi*rad*rad

    # convert to mL and mm
    return wall_volume*1e6, surf_area*1e6

# ---------------------------------------------------------------------------
def setup_windkessel_parameters(args):
    """ set Windkessel parameters for the different ventricles """
    if args.cavity == 'LV':
        wk3 = ['-lv_wk3.name', 'Aorta']
        if args.fast:
            wk3 += ['-lv_wk3.R1', '0.12',
                    '-lv_wk3.R2', '1.40',
                    '-lv_wk3.C', '0.11']
        else:
            wk3 += ['-lv_wk3.R1', '0.1',
                    '-lv_wk3.R2', '0.3',
                    '-lv_wk3.C', '5.0']

        # set valve parameters
        wk3 += ['-aortic_valve.Rfwd', 0.01]
        wk3 += ['-mitral_valve.Rfwd', 0.05]

    elif args.cavity == 'RV':
        wk3 = ['-lv_wk3.name', 'PulmArtery']

        if args.fast:
            wk3 = ['-rv_wk3.R1', '0.2',
                   '-rv_wk3.R2', '4.0',
                   '-rv_wk3.C', '0.375']
        if args.cavity == 'RV':
            wk3 = ['-rv_wk3.R1', '0.2',
                   '-rv_wk3.R2', '4.00',
                   '-rv_wk3.C', '0.375']

        # set valve parameters
        wk3 += ['-pulmonic_valve.Rfwd', 0.05]
        wk3 += ['-tricuspid_valve.Rfwd', 0.10]
    else:
        raise Exception('Cavity not yet implemented.')
    return wk3

# =============================================================================
#    Visualization functions
# =============================================================================

# --- visualization settings -------------------------------------------------
def setup_visualization(args):
    """ setup cardiovascular system

    Args:
        args: parser arguments

    Returns:
        A list of visualization options.
    """
    vis_opts = ['-gridout_i', 3]
    if args.visualize:
        vis_opts += ['-mech_output', 1+2,
                     '-mech_output_domain', 1]

        # Switch off passive stress calculation
        if args.pstress_off:
            vis_opts += ['-stress_value', 0]
        else:
            vis_opts += ['-stress_value', 16,        # 4: 1st principal on nodes
                         '-strain_value', 0,         # 1: fiber strain on nodes
                         '-mech_pp_stress_mode', 4,  # 1: passive stress
                         '-work_value', 1]           # 1: work on nodes

    elif args.postprocess:
        # get the same simulation parameters for postprocessing
        parameter_file = args.ID + "/parameters.par"
        pp_spacedt = 1
        vis_opts += ['+F', parameter_file,
                     '-simID', args.ID]
        vis_opts += ['-mech_pp_reference', 0]
        # overwrite output parameters
        vis_opts += ['-mech_output', 1+2,
                     '-mech_output_domain', 1,
                     '-vtk_output_mode', 4,
                     '-spacedt', pp_spacedt,  # time between spacial output [ms]
                     '-gridout_i', 3]

        # Switch off passive stress calculation
        if args.pstress_off:
            vis_opts += ['-stress_value', 0]
        else:
            vis_opts += ['-stress_value', 16+4,      # 4: 1st principal on nodes
                         '-mech_pp_stress_mode', 4,  # 1: total stress
                         '-strain_value', 16+4,      # 1: fiber strain on nodes
                         '-work_value', 1]           # 1: work on nodes
    else:
        vis_opts += ['-mech_output', 1]  # 1: default igb vector output

    return vis_opts

# --- meshalyzer visualization ------------------------------------------------
def visualize_meshalyzer(job, args, mesh_name):
    """ visualize with Meshalyzer

    Args:
        job: job definition
        args: parser arguments
        mesh_name: mesh name
    """

    mesh = os.path.basename(mesh_name)
    geom = os.path.join(job.ID, mesh + '_i')
    data = os.path.join(job.ID, 'Tension.igb')
    deform = os.path.join(job.ID, 'x.dynpt')

    if args.mechanics_off:
        data = os.path.join(job.ID, 'vm.igb')
        job.gunzip(data)
        job.meshalyzer(geom, data)

    else:
        job.gunzip(deform)
        job.meshalyzer(geom, data, deform)


# =============================================================================
#    TESTS
# =============================================================================

__tests__ = []
F_TAGS = [testing.tag.FAST, testing.tag.MECHANICS, testing.tag.PARALLEL]
M_TAGS = [testing.tag.MEDIUM, testing.tag.MECHANICS, testing.tag.PARALLEL]

# coupling to Windkessel
ACTIVE_F_PV_WK = testing.Test('ring-pv-loop-wk-fast', run,
                              ['--duration', '300', '--np', '2', '--fast',
                               '--cvs-model', 'wk3', '--s_peak', '120',
                               '--experiment', 'active-pv-loop'],
                              tags=F_TAGS)
__tests__.append(ACTIVE_F_PV_WK)

ACTIVE_M_PV_WK = testing.Test('ring-pv-loop-wk-medium', run,
                              ['--duration', '500', '--np', '8',
                               '--cvs-model', 'wk3', '--s_peak', '150',
                               '--experiment', 'active-pv-loop'],
                              tags=M_TAGS)
__tests__.append(ACTIVE_M_PV_WK)

# coupling to CircAdapt
ACTIVE_F_PV_CA = testing.Test('ring-pv-loop-CA-fast', run,
                               ['--duration', '300', '--np', '4', '--fast',
                                '--cvs-model', 'circadapt', '--s_peak', '120',

                                '--experiment', 'active-pv-loop'],
                               tags=F_TAGS)
__tests__.append(ACTIVE_F_PV_CA)

ACTIVE_MS_PV_CA = testing.Test('ring-pv-loop-CA-coarse-mesh', run,
                              ['--beats', '25', '--np', '4', '--fast',
                               '--mech-with-inertia', '--stress', 'landhuman',
                               '--cvs-model', 'circadapt', '--s_peak', '170',
                               '--ldOn',
                               '--experiment', 'active-pv-loop'],
                              tags=M_TAGS)
__tests__.append(ACTIVE_MS_PV_CA)

ACTIVE_M_PV_CA = testing.Test('ring-pv-loop-CA-medium-mesh', run,
                              ['--beats', '25', '--np', '8',
                              '--mech-with-inertia', '--stress', 'landhuman',
                              '--cvs-model', 'circadapt', '--s_peak', '200',
                              '--ldOn',
                              '--experiment', 'active-pv-loop'],
                             tags=M_TAGS)
__tests__.append(ACTIVE_M_PV_CA)

# active inflate
ACTIVE_INF = testing.Test('ring-active-inflate', run,
                          ['--duration', '100', '--fast', '--np', '2', '--preload', '10',
                           '--s_peak', '50', '--experiment', 'active-inflate'],
                          tags=F_TAGS)
__tests__.append(ACTIVE_INF)

for test in __tests__:
    test.add_filecmp_check('cav.LV.csv', testing.max_error, 0.01)

# ACTIVE FREE
ACTIVE_F = testing.Test('ring-active-fast', run,
                        ['--duration', '100', '--fast', '--np', '2'],
                        tags=F_TAGS)
__tests__.append(ACTIVE_F)

ACTIVE_M = testing.Test('ring-active-medium', run,
                        ['--duration', '300', '--np', '8', '--s_peak', '40'],
                        tags=M_TAGS)
__tests__.append(ACTIVE_M)


for test in __tests__:
    test.add_filecmp_check('x.dynpt.gz', testing.max_error, 0.05)

# =============================================================================
if __name__ == '__main__':
    run()
