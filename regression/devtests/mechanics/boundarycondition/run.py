#!/usr/bin/env python

"""
This example demonstrates the application of different types of boundary
conditions to a cube.

Mesh
====

A simple cube mesh is generated of default side length 0.2 mm and resolution
0.l mm. This can be changed with command line parameters.

Boundary Conditions
===================

In both experiments, the :math:`y = -0.5 \\times sidelength` surface is fixed
with a Dirichlet boundary condition, and the :math:`y = 0.5 \\times sidelength`
surface is either displaced with a time-varying Dirichlet boundary condition,
or has a time-varying pressure applied with a Neumann boundary condition. The
magnitude of these displacements/pressure can be modified on the command line.
"""
import os
from datetime import date
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import model
from carputils import testing

EXAMPLE_DESCRIPTIVE_NAME = "Mechanics Boundary Conditions"
EXAMPLE_AUTHOR = ("Christoph Augustin <christoph.augustin@medunigraz.at> and"
                  "Matthias Gsell <matthias.gsell@medunigraz.at>")
EXAMPLE_DIR = os.path.dirname(__file__)

# -----------------------------------------------------------------------------
def generate_parser():
    """ generate command line parser """
    parser = tools.standard_parser()
    parser.add_argument('--experiment',
                        default='dirichlet',
                        choices=['dirichlet', 'neumann'],
                        help='pick experiment type')
    parser.add_argument('--fixation',
                        default='strong',
                        choices=['strong', 'weak'],
                        help='choose fixation of the bottom')
    parser.add_argument('--sidelength',
                        type=float, default=0.2,
                        help='choose sidelength of the cube in mm')
    parser.add_argument('--resolution',
                        type=float, default=0.1,
                        help='choose mesh resolution in mm')
    parser.add_argument('--magnitude',
                        type=float,
                        help='magnitude of BC to apply (kPa for neumann, mm '
                             'for dirichlet)')
    parser.add_argument('--tend',
                        type=float, default=500.,
                        help='duration of simulation (ms)')
    parser.add_argument('--hexahedra',
                        type=str, default='off', choices=['on', 'off'],
                        help='Switch to hexahedral elements.')
    return parser

# -----------------------------------------------------------------------------
def job_id(args):
    """ Generate name of top level output directory. """
    today = date.today()
    tpl = '{}_{}_{}_{}_np{}'
    if args.mech_with_inertia :
        tpl = '{}_{}_{}_{}_GA_np{}'
    return tpl.format(today.isoformat(), args.experiment,
                                     args.mech_element, args.flv, args.np)

# -----------------------------------------------------------------------------
@tools.carpexample(generate_parser, job_id)
def run(args, job):
    """ setup command line options for carpentry """

    # Generate mesh - Units are mm
    if args.hexahedra == 'on':
        etype = 'hexa'
    else:
        etype = 'tetra'
    s_l = args.sidelength
    geom = mesh.Block(size=(s_l,s_l,s_l), resolution=args.resolution, etype=etype)

    # Set fibre angle to 0, sheet angle to 90
    geom.set_fibres(0,0, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)


    # Material definitions
    if args.mech_element == 'P1-P0':
        kappa = 500       # penalty formulation
    else:
        kappa = 1e100      # nearly incompressible

    material = model.mechanics.NeoHookeanMaterial([0], 'tissue',
                                                        kappa=kappa, c=7.345)
    if args.resolution < 0.02: # for finer meshes we take another material
        material = model.mechanics.GuccioneMaterial([0], 'tissue',
                                                          kappa=kappa)

    # Switch off EP
    ep_opts = ['-num_imp_regions',       1,
               '-imp_region[0].name',    'passive',
               '-imp_region[0].im',      'PASSIVE',
               '-imp_region[0].num_IDs', 100,
               '-num_stim',              0]


    # Build command line
    # Get basic command line, including solver options
    cmd = tools.carp_cmd()
    cmd += tools.gen_physics_opts(MechTags=[1], IntraTags=[1])

    # Add timers and options
    tstep = min(5.0, args.tend)
    cmd += ['-simID',                    job.ID,
            '-meshname',                 meshname,
            '-timedt',                   tstep,
            '-mechDT',                   tstep,
            '-spacedt',                  tstep,
            '-tend',                     args.tend]

    if args.mech_with_inertia:
        cmd += ['-mech_activate_inertia', 1]
        cmd += ['-mech_stiffness_damping', 0.0]
        cmd += ['-mech_rho_inf', 0.5]

    # volume tracking
    cmd += ['-volumeTracking',     1,
            '-numElemVols',        1,
            '-elemVols[0].name',   'block']

    if args.mech_element != 'P1-P0':
        cmd += ['-mech_vol_function', '1']

    # Switch off EP
    cmd += ep_opts

    # add material options
    cmd += model.optionlist([material])

    # add boundary conditions
    cmd += boundary_condition(args, geom)

    # output options
    cmd +=  ['-mech_output',        1,   # 1 igb
             '-stress_value',       4]   # 1st principal stress mapped on nodes

    if args.visualize:
        cmd += ['-gridout_i',  3,
                '-mech_output', 1+2]

    # Run simulation
    job.carp(cmd)

    # Do meshalyzer visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        view = 'bc.mshz'
        data = os.path.join(job.ID, 'firstPrincipalStress.igb.gz')
        deform = os.path.join(job.ID, 'x.dynpt')

        # Unzip data
        job.gunzip(deform)

        # Call meshalyzer
        job.meshalyzer(geom, data, deform, view)

# -----------------------------------------------------------------------------
def boundary_condition(args, geom):
    """ setup boundary conditions """
    if args.experiment == 'dirichlet':

        displ = 50 if args.magnitude is None else args.magnitude*1000
        spring = 0.25
        dpspring = 0.01

        if args.fixation == 'strong':

            dbcy0 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'y', True)
            dbcy1 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 1, 'y', False)

            bc_opts = ['-num_mechanic_dbc', 2]

            # Fix bottom stronlgy
            bc_opts += dbc_homogeneous(0, 'bottom') + dbcy0

            # Move top
            bc_opts += dbc_inhomogeneous(1, 'top', displ) + dbcy1

        elif args.fixation == 'weak':  # weak fixation

            nbcy0 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'y', True)
            dbcy1 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'y', False)

            # Fix bottom weakly
            if args.mech_with_inertia:
                bc_opts = ['-num_mechanic_bs', 2]
                bc_opts += boundary_spring(0, spring)
                bc_opts += boundary_spring(1, dpspring)
                bc_opts += ['-num_mechanic_nbc', 1]
                bc_opts += neumann_bc(0, 'bottom', 0.0, 0, 1) + nbcy0
            else:
                bc_opts = ['-num_mechanic_bs', 1]
                bc_opts += boundary_spring(0, spring)
                bc_opts += ['-num_mechanic_nbc', 1]
                bc_opts += neumann_bc(0, 'bottom', 0.0, 0, -1) + nbcy0

            bc_opts += ['-num_mechanic_dbc', 1]
            # Move top
            bc_opts += dbc_inhomogeneous(0, 'top', displ) + dbcy1

    elif args.experiment == 'neumann':

        pressure = 10 if args.magnitude is None else args.magnitude
        spring = 0.25
        dpspring = 0.0001

        if args.fixation == 'strong':
            dbcy0 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'y', True)
            nbcy1 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'y', False)

            # Fix bottom strongly
            bc_opts = ['-num_mechanic_dbc', 1]
            bc_opts += dbc_homogeneous(0, 'bottom') + dbcy0

            # Apply pressure to top
            bc_opts += ['-num_mechanic_nbc', 1]
            bc_opts += neumann_bc(0, 'top', pressure, -1, -1) + nbcy1

        elif args.fixation == 'weak':

            nbcy0 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'y', True)
            nbcy1 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 1, 'y', False)
            # Fix bottom weakly
            if args.mech_with_inertia:
                bc_opts = ['-num_mechanic_bs', 2]
                bc_opts += boundary_spring(0, spring)
                bc_opts += boundary_spring(1, dpspring)
                bc_opts += ['-num_mechanic_nbc', 2]
                bc_opts += neumann_bc(0, 'bottom', 0.0, 0, 1) + nbcy0
            else:
                bc_opts = ['-num_mechanic_bs', 1]
                bc_opts += boundary_spring(0, spring)
                bc_opts += ['-num_mechanic_nbc', 2]
                bc_opts += neumann_bc(0, 'bottom', 0.0, 0, -1) + nbcy0
            # Apply pressure to top
            bc_opts += neumann_bc(1, 'top', pressure, -1, -1) + nbcy1

    return bc_opts

# -----------------------------------------------------------------------------
def dbc_homogeneous(idx, name):
    """
    Generate homogeneous Dirichlet boundary condition.
    """

    base = f'-mechanic_dbc[{idx}].'

    dbc = [base + 'name',       name,
           base + 'bctype',     0,
           base + 'apply_ux',   1,
           base + 'apply_uy',   1,
           base + 'apply_uz',   1,
           base + 'dump_nodes', 1]

    return dbc

def dbc_inhomogeneous(idx, name, magnitude):
    """
    Generate inhomogeneous Dirichlet boundary condition.
    """

    base = f'-mechanic_dbc[{idx}].'
    trace = 'traces/dbc_cube'
    direction = 'y'

    dbc = [base + 'name',     name,
           base + 'bctype',   1,
           base + 'apply_ux', 1,
           base + 'apply_uy', 1,
           base + 'apply_uz', 1]

    for coord in ['x', 'y', 'z']:
        dbc += [base + 'u' + coord, magnitude if coord == direction else 0]

    dbc += [base + f'u{direction}_file', trace,
            base + 'dump_nodes',         1]

    return dbc


def boundary_spring(idx, spring):
    """
    Generate boundary spring.
    """

    base = f'-mechanic_bs[{idx}].'

    b_s = [base + 'value',   spring,
          base + 'edidx',   -1]

    return b_s

def neumann_bc(idx, name, magnitude, spring_idx, dpspring_idx):
    """
    Generate Neumann boundary condition.
    """

    base = f'-mechanic_nbc[{idx}].'
    trace = 'traces/nbc_cube'

    nbc = [base + 'name',            name,
           base + 'pressure',        magnitude,
           base + 'trace',           trace,
           base + 'spring_idx',      spring_idx]
    if dpspring_idx >= 0:
        nbc += [base + 'dpspring_idx', dpspring_idx]

    return nbc


# Generate tests
__tests__ = []
MESH_T_L = ['tet', 'hex']  # mesh element type list
TIME_S_L = ['GA', 'QS']    # time stepping scheme list
for exp in ['dirichlet', 'neumann']:
    MESH_T_L[0], MESH_T_L[1] = MESH_T_L[1], MESH_T_L[0]  # swap elements
    for fix in ['strong', 'weak']:
        TIME_S_L[0], TIME_S_L[1] = TIME_S_L[1], TIME_S_L[0]  # swap elements
        for mesh_t, time, FE in zip(MESH_T_L, TIME_S_L, ['P1-P0', 'MINI']):
            if FE == 'P1-P0':
                T_END   = 500
            else:
                T_END   = 100
            if mesh_t == 'tet':
                HEX_ON = 'off'
            else:
                HEX_ON = 'on'

            experiment_string = ['--experiment',   exp,
                                 '--mech-element', FE,
                                 '--tend',         T_END,
                                 '--fixation',     fix,
                                 '--hexahedra',    HEX_ON]
            if time == 'GA':
                experiment_string += ['--mech-with-inertia']

            # Generate fast test
            F_test = testing.Test(f'F{exp}_fixation_{fix}_{FE}_{mesh_t}_{time}',
                                  run, experiment_string,
                                  tags=[testing.tag.FAST,
                                        testing.tag.MECHANICS,
                                        testing.tag.SERIAL])
            __tests__.append(F_test)

            experiment_string += ['--np',         '4',
                                  '--resolution', '0.02']

            # Generate medium test
            M_test = testing.Test(f'M{exp}_fixation_{fix}_{FE}_{mesh_t}_{time}',
                                  run, experiment_string,
                                  tags=[testing.tag.MEDIUM,
                                        testing.tag.MECHANICS,
                                        testing.tag.PARALLEL])
            __tests__.append(M_test)

# Add file checks
for t in __tests__:
    t.add_filecmp_check('x.dynpt.gz', testing.max_error, 0.01)
    t.add_filecmp_check('firstPrincipalStress.igb.gz', testing.max_error, 0.01)

if __name__ == '__main__':
    run()
