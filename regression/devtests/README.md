# openCARP Developer Tests

This git repository contains developer tests for openCARP.
Running these tests requires the 'carputils' Python package;
please see the documentation for installation and usage instructions:

http://git.opencarp.org/openCARP/carputils
