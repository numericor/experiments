#!/usr/bin/env python3

"""
Monodomain and bidomain test cases with analytical solutions as suggested in
Pathmanathan, P., and Gray, R.A. (2014). 
"Verification of computational models of cardiac electro-physiology"
Int J Numer Method Biomed Eng 30, 525–544.
doi:10.1002/cnm.2615

We recast eq. (20) in the openCARP manual from
:math:`\\nabla \\cdot \\left( \\hat{\\sigma}_m \\nabla V_m \\right) = \\beta I_{ion} + \\beta C_m \\frac{dV_m}{dt}`
to
:math:`\\nabla \\cdot \\left( \\frac{\\hat{\\sigma}_m}{C_m} \\nabla V_m \\right) = \\frac{\\beta}{C_m} I_{ion} + \\beta \\frac{dV_m}{dt}`
so that we can take care of :math:`C_m` when defining :math:`\\sigma` and :math:`I_{ion}`.

See chapter 3.4 of the openCARP manual for more information regarding units.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Analytical tests'
EXAMPLE_AUTHOR = 'Axel Loewe <axel.loewe@kit.edu>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

# import required carputils modules
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing
from carputils import carpio
import math
import numpy as np
import matplotlib.pyplot as plt

# define parameters exposed to the user on the commandline
def parser():
    parser = tools.standard_parser()
    parser.add_argument('--res',
                        type=float, default=0.02,
                        help='Resolution of the mesh to be generated (in mm)')    
    parser.add_argument('--dim',
                        type=int, default=3,
                        help='Dimensionality of the test problem (so far only 3D is supported)')
    parser.add_argument('--mode',
                        type=str, default="monodomain",
                        help='Choose between mondomain, bidomain and bidomain-bath (so far only monodomain and bidomain are supported)')    
    parser.add_argument('--tend',
                        default=1,
                        help='simulated time (ms)')
    parser.add_argument('--dt',
                        default=1,
                        help='time step for calculation (us)')
    parser.add_argument('--downsample-output-time',
                        type=float, default=100,
                        help='output every Nth timestep')
    parser.add_argument('--parab-solve',
                        type=int, default=1,
                        help='Time integration scheme: explicit (0), Crank-Nicolson (1), second order implicit (2)')    
    parser.add_argument('--verbose-output',
                        action='store_true',
                        help='Output igb files of state variables and differences as well as initial data')    
    parser.add_argument('--analyze-convergence',
                        action='store_true',
                        help='Analyze spatial convergence [0.015,0.1]mm')    
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_analytic_{}_{}D'.format(today.isoformat(), args.mode,
                                         args.dim,)

@tools.carpexample(parser, jobID)
def run(args, job):

    if args.mode == "monodomain":
        bidomain = 0
        bath = 0
    elif args.mode == "bidomain":
        bidomain = 1
        bath = 0
    elif args.mode == "bidomain-bath":
        bidomain = 1
        bath = 1
        raise Exception("Mode " + args.mode + " not fully implemented.")
    else:
        raise Exception("Mode " + args.mode + " not supported.")
    
    cmd = tools.carp_cmd()
    cmd += [ '-num_imp_regions',   1,
            '-imp_region[0].im',  'Pathmanathan',
            '-bidomain',          bidomain,
            '-timedt',            1,
            '-parab_solve',       args.parab_solve,
            '-dt',                1,
            '-spacedt',           args.downsample_output_time /1000.,
          ]
    
    # Attach electrophysiology physics (monodomain) to mesh region with tag 1
    cmd += tools.gen_physics_opts(IntraTags=[1])

    cmd += ['-simID',    job.ID,
            '-tend',     args.tend]

    # Set conductivities
    cmd += ['-num_gregions',			1,
    		'-gregion[0].name', 		"myocardium",
    		'-gregion[0].num_IDs', 	1,  		# one tag will be given in the next line
    		'-gregion[0].ID', 		"1",		# use these setting for the mesh region with tag 1
    		'-imp_region[0].cellSurfVolRatio',   3e-3 # e-3 to convert from 1/mm to 1/um
    		]
    if bidomain: # /200. to take care of Cm (200uF/cm^2); *1e-3 to get from muS/mm to S/m
        cmd += ['-gregion[0].g_il',         1.1 * math.pi**-2,  # intracellular conductivity in longitudinal direction
                '-gregion[0].g_it',         1.2 * math.pi**-2,  # intracellular conductivity in transverse direction
                '-gregion[0].g_in',         0.3 * math.pi**-2,  # intracellular conductivity in sheet direction
                '-gregion[0].g_el',         1.-(1./math.sqrt(2)) * math.sqrt(2) * (1.1 * math.pi**-2),  # extracellular conductivity in longitudinal direction
                '-gregion[0].g_et',         1.-(1./math.sqrt(2)) * math.sqrt(2) * (1.2 * math.pi**-2),  # extracellular conductivity in transverse direction
                '-gregion[0].g_en',         1.-(1./math.sqrt(2)) * math.sqrt(2) * (0.3 * math.pi**-2),  # extracellular conductivity in sheet direction
                '-gregion[0].g_mult',       1e-3 / 200., # * /200. to take care of Cm (200uF/cm^2); *1e-3 to get from muS/mm to S/m
                ]    
        if bath:
            cmd += ['-imp_region[0].im_param',          'beta="-1.1*(1.-1./sqrt(2)),xi=3"']
            sb = 1 # to be fixed
        else:
            cmd += ['-imp_region[0].im_param',          'beta="-8.6*(1.-1./sqrt(2)),xi=3"']
    else: # monodomain
        cmd += ['-gregion[0].g_il',         1.1 * math.pi**-2, 
                '-gregion[0].g_it',         1.2 * math.pi**-2,  
                '-gregion[0].g_in',         0.3 * math.pi**-2,  
                '-gregion[0].g_el',         1.1 * math.pi**-2,
                '-gregion[0].g_et',         1.2 * math.pi**-2,
                '-gregion[0].g_en',         0.3 * math.pi**-2,
                '-gregion[0].g_mult',       2 * 1e-3 / 200.,    # *2 to get monodomain conductivity right, /200. to take care of Cm (200uF/cm^2); *1e-3 to get from muS/mm to S/m
                '-imp_region[0].im_param',   'beta="-8.6,xi=3"',   
                ] 

    if args.verbose_output:
        cmd += [
            '-adjustment[0].dump',      1,
            '-adjustment[1].dump',      1,
            '-adjustment[2].dump',      1,        
            '-num_gvecs',         3,
            '-gvec[0].name',     'u1',
            '-gvec[0].ID[0]',    'u1',
            '-gvec[1].name',     'u2',
            '-gvec[1].ID[0]',    'u2',
            '-gvec[2].name',     'u3',
            '-gvec[2].ID[0]',    'u3',
            ]

    if args.visualize and not args.analyze_convergence:
        cmd += ['-gridout_i', 3]    # output both surface & volumetric mesh for visualization
        if bath:
            cmd += ['-gridout_e', 3]    # output both surface & volumetric mesh for visualization

    if args.analyze_convergence:
        resolutions = [0.015, 0.025, 0.05, 0.1]
    else:
        resolutions = [args.res]
    linfs = []

    for res in resolutions:
        # Generate unit block mesh, units in mm
        geom = mesh.Block(size=(1, 1, 1), centre=(0.5,0.5,0.5), resolution=res)
        if bath:
            # caveat: the benchmark problem includes only bath at the front 
            # and back but not above and below
            geom.set_bath(thickness=1)

        geom.set_fibres(0, 0, 90, 90)

        # Generate and return base name
        meshname = mesh.generate(geom)

        cmd_ = cmd + [
        '-meshname', meshname,
        '-num_adjustments',         3,
        '-adjustment[0].file',      meshname + '_u1_init.adj',
        '-adjustment[0].variable',  'Pathmanathan.u1',
        '-adjustment[1].file',      meshname + '_u2_init.adj',
        '-adjustment[1].variable',  'Pathmanathan.u2',
        '-adjustment[2].file',      meshname + '_Vm_init.adj',
        '-adjustment[2].variable',  'Vm',        
        ]


        # construct intial states
        with open(meshname + '.pts', 'r') as ptsfile:
            nNodes = ptsfile.readline()  
            with open(meshname + '_u1_init.adj', 'w') as u1:
                u1.write(nNodes)
                u1.write("intra\n")
                with open(meshname + '_u2_init.adj', 'w') as u2:
                    u2.write(nNodes)
                    u2.write("intra\n")
                    with open(meshname + '_Vm_init.adj', 'w') as Vm:
                        Vm.write(nNodes)
                        Vm.write("intra\n")
                        lines = ptsfile.readlines()
                        iNode = 0
                        for line in lines:
                            [x,y,z] = [float(v)/1000. for v in line.split()]
                            if args.dim == 3:                        
                                F = math.cos(math.pi * x) * math.cos(2*math.pi*y) * math.cos(3*math.pi * z)
                                G = 1 + x * y**2 * z**3
                            elif args.dim == 2:
                                F = math.cos(math.pi * x) * math.cos(2*math.pi*y)
                                G = 1 + x * y**2 
                                raise Exception("2D mode not fully implemented.")
                            elif args.dim == 1:
                                F = math.cos(math.pi * x)
                                G = 1 + x      
                                raise Exception("1D mode not fully implemented.")                       
                            else:
                                raise Exception("Dimensionality " + args.dim + " not supported")
                            u1.write(str(iNode) + " " + str(G + F) + "\n")
                            u2.write(str(iNode) + " " + str(1 / math.sqrt(G)) + "\n")
                            Vm.write(str(iNode) + " " + str(F) + "\n")                        
                            iNode += 1

        # Run simulation
        job.carp(cmd_)

        # create reference solution
        igb = carpio.igb.IGBFile(job.ID + '/vm.igb')
        vm = igb.data()
        tInc = igb.header()['inc_t']
        nTimesteps = int(igb.header()['dim_t'] / tInc + 1)
        with open(meshname + '.pts', 'r') as ptsfile:
            nNodes = int(ptsfile.readline()) 
            refVm = np.empty(nNodes*nTimesteps, dtype=np.single)
            refPhie = np.empty(nNodes*nTimesteps, dtype=np.single)
            refu1 = np.empty(nNodes*nTimesteps, dtype=np.single)
            refu2 = np.empty(nNodes*nTimesteps, dtype=np.single)
            diffVm = np.empty(nNodes*nTimesteps, dtype=np.single)
            lines = ptsfile.readlines()
            iNode = 0
            for line in lines:
                [x,y,z] = [float(v)/1000. for v in line.split()]
                if args.dim == 3:                        
                    F = math.cos(math.pi * x) * math.cos(2.*math.pi*y) * math.cos(3.*math.pi * z)
                    G = 1.+ x * y**2 * z**3
                elif args.dim == 2:
                    F = math.cos(math.pi * x) * math.cos(2.*math.pi*y)
                    G = 1. + x * y**2 
                elif args.dim == 1:
                    F = math.cos(math.pi * x)
                    G = 1. + x                             
                else:
                    raise Exception("Dimensionality " + args.dim + " not supported")
                for iTimestep in range(nTimesteps):
                    refVm[iNode + iTimestep * nNodes] = math.sqrt(1. + iTimestep*tInc) * F
                    refPhie[iNode + iTimestep * nNodes] = - math.sqrt(1. + iTimestep*tInc) * F / math.sqrt(2)
                    diffVm[iNode + iTimestep * nNodes] = vm[iNode + iTimestep * nNodes] - refVm[iNode + iTimestep * nNodes]
                    if args.verbose_output:
                        refu1[iNode + iTimestep * nNodes] = (1. + iTimestep*tInc) * G + math.sqrt(1. + iTimestep*tInc) * F
                        refu2[iNode + iTimestep * nNodes] = 1./ ((1. + iTimestep*tInc) * math.sqrt(G))                
                iNode += 1
        igb.close()
        igbOut = carpio.igb.IGBFile(job.ID + '/ref_Vm.igb', 'w')
        igbOut.write(refVm, igb.header())
        igbOut.close()
        igbOut = carpio.igb.IGBFile(job.ID + '/ref_Phie.igb', 'w')
        igbOut.write(refPhie, igb.header())
        igbOut.close()
        igbOut = carpio.igb.IGBFile(job.ID + '/diff_Vm.igb', 'w')
        igbOut.write(diffVm, igb.header())
        igbOut.close()

        linf = 0
        for i in range(nTimesteps):
            linf = max(linf, np.linalg.norm(np.reshape(diffVm, (nNodes,nTimesteps))[:,i]))
        linf *= res**3
        print("L^inf(L^2) error for resolution %.3fmm: %e"%(res, linf))
        linfs.append(linf)            

        if args.verbose_output:
            igbOut = carpio.igb.IGBFile(job.ID + '/ref_u1.igb', 'w')
            igbOut.write(refu1, igb.header())
            igbOut.close()
            igbOut = carpio.igb.IGBFile(job.ID + '/ref_u2.igb', 'w')
            igbOut.write(refu2, igb.header())
            igbOut.close()        
            diffu1 = np.empty(nNodes*nTimesteps, dtype=np.single)
            diffu2 = np.empty(nNodes*nTimesteps, dtype=np.single)
            igbu1 = carpio.igb.IGBFile(job.ID + '/u1.igb')
            u1 = igbu1.data()
            igbu2 = carpio.igb.IGBFile(job.ID + '/u2.igb')
            u2 = igbu2.data()
            igbu3 = carpio.igb.IGBFile(job.ID + '/u2.igb')
            u3 = igbu3.data()
            for iNode in range(nNodes):
                for iTimestep in range(nTimesteps):
                    diffu1[iNode + iTimestep * nNodes] = u1[iNode + iTimestep * nNodes] - refu1[iNode + iTimestep * nNodes]
                    diffu2[iNode + iTimestep * nNodes] = u2[iNode + iTimestep * nNodes] - refu2[iNode + iTimestep * nNodes]
            igbu1.close()
            igbu2.close()
            igbu3.close()    
            igbOut = carpio.igb.IGBFile(job.ID + '/diff_u1.igb', 'w')
            igbOut.write(diffu1, igb.header())
            igbOut.close()    
            igbOut = carpio.igb.IGBFile(job.ID + '/diff_u2.igb', 'w')
            igbOut.write(diffu2, igb.header())    
            igbOut.close()     

    print("Resolutions: " + str(resolutions))
    print("L^inf(L^2) errors: " + str(linfs))

    if args.analyze_convergence:
        plt.plot(resolutions, linfs, '-x') 
        plt.xlabel('resolution (mm)')
        plt.ylabel('L^inf(L^2) error) (mV)')
        plt.xscale('log')
        plt.yscale('log')
        plt.savefig(os.path.join(job.ID, 'convergence.png'))
          
    if args.visualize and args.analyze_convergence:
        plt.show()

    # Do visualization
    if args.visualize and not settings.platform.BATCH and not args.analyze_convergence:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        # display trensmembrane voltage
        data = os.path.join(job.ID, 'diff_vm.igb')
        
        # load predefined visualization settings
        view = os.path.join(EXAMPLE_DIR, 'block.mshz')

        # Call meshalyzer
        job.meshalyzer(geom, data, view)

# test settings
test_3D_monodomain = testing.Test('3D_monodomain', run, ['--np','8' ],
                             tags=[testing.tag.MEDIUM,
                                   testing.tag.BIDOMAIN,
                                   testing.tag.PARALLEL])

# Could be changed to an analytic test (or a newly implemented test directly considering the norm provided above)
# Would need some refactoring, though.
test_3D_monodomain.add_filecmp_check('vm.igb', testing.max_error, 0.001)

__tests__ = [test_3D_monodomain]



if __name__ == '__main__':
    run()
