#!/usr/bin/env python3

"""
Run an Eikonal simulation.
"""

EXAMPLE_DESCRIPTIVE_NAME = 'Eikonal Activation'
EXAMPLE_AUTHOR = ('Gernot Plank <gernot.plank@medunigraz.at> '
                  'and Aurel Neic <aurel.neic@medunigraz.at>')

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()

    parser.add_argument('--propagation',
                        default='eikonal', choices=['eikonal', 'R-D', 'R-E'],
                        help='pick propagation model')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_{}_{}_np{}'
    return tpl.format(today.isoformat(), args.propagation,
                      args.flv.upper(), args.np)


def get_stim(args):
  stim_par = []
  nstim = 2

  #electrical setup
  prefix = '-stim[0]'
  stim_par += [prefix+'.name',           "Left",
               prefix+'.crct.type',      0,
               prefix+'.pulse.strength', 250.0,
               prefix+'.ptcl.duration',  2.,
               prefix+'.ptcl.start',     2.,
               prefix+'.ptcl.npls',      1]

  prefix = '-stim[1]'
  stim_par += [prefix+'.name',           "Right",
               prefix+'.crct.type',       3]

  if args.propagation == 'R-E':
    nstim = 3
    prefix = '-stim[2]'
    stim_par += [prefix+'.name',           "takeoff",
                 prefix+'.crct.type',       8]


  return ['-num_stim', nstim] + stim_par

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(10, 0.1, 0.1))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Add region on right hand side
    right = mesh.BoxRegion((0, -0.1, -0.1), (10, 0.1, 0.1), tag=2)
    geom.add_region(right)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # define default view settings
    view = 'stimulation_V.mshz'

    # Add all the non-general arguments
    cmd = tools.carp_cmd('eikonal.par')

    cmd += tools.gen_physics_opts(IntraTags=[1,2], EikonalTags=[1,2])

    cmd += ['-meshname', meshname,
            '-pstrat', 2,
            '-simID',    job.ID]

    cmd += get_stim(args)
    cmd += mesh.block_bc_opencarp(geom, 'stim', 0, 'x', False)

    if args.propagation == 'eikonal':
        cmd += ['-experiment', 6]

    if args.visualize:
        vis_cmd = ['-gridout_i', 2]
        cmd += vis_cmd

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')

        if args.propagation == 'eikonal':
            data = os.path.join(job.ID, 'vm_act_seq.dat')
        else:
            data = os.path.join(job.ID, 'init_acts_vm_act-thresh.dat')

        view = 'eikonal.mshz'

        job.meshalyzer(geom, data, view)

# test eikonal
test_eikonal = testing.Test('eikonal',
                            run, ['--propagation', 'eikonal'],
                            tags=[testing.tag.FAST,
                                  testing.tag.SERIAL,
                                  testing.tag.EIKONAL])
test_eikonal.add_filecmp_check('vm_act_seq.dat', testing.max_error, 0.001)

__tests__ = [test_eikonal]

if __name__ == '__main__':
    run()
