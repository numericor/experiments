#!/usr/bin/env python3

"""
This is an example that shall help tuning related parameters to obtain the
desired velocity in the purkinje network.

Problem Setup
=============

This example defines a minimalistic purkinje network attached on top of a thin
cuboid on the domain (units in microns):

.. math::

    0.0  \leq x \leq 3 * dx3D

    0.0 \leq y \leq 10000

    0.0 \leq z \leq 1 * dx3D

And applies an electrical HIS stimulus on the upper side of the cuboid:

.. image:: /images/cv_purkinje_setup.png
    :scale: 60%

Conduction velocity is measured 500um after the cable begin and
500um before the cable end.

Usage
=====

Following optional arguments with their default values are available:

.. code-block:: bash

    ./run.py --sourceModel = monodomain
             --propagation = R-D
             --dx3D        = 200.
             --resist      = 100.
             --cond        =   0.0006
             --effcond     =   1
             --dx1D        = 100.
             --tend        =  10.
             --suffix      = ''

As with other examples, add the ``--visualize`` option to automatically load
the results in meshalyzer:

.. code-block:: bash

    ./run.py --visualize
"""

from __future__ import print_function
import os
import sys
import math
import numpy as np
from datetime import date

from carputils import ep
from carputils import mesh
from carputils import settings
from carputils import testing
from carputils import tools

EXAMPLE_DESCRIPTIVE_NAME = 'Conduction Velocity Purkinje'
EXAMPLE_AUTHOR = 'Anton Prassl <anton.prassl@medunigraz.at>'

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--sourceModel',
                        default = 'monodomain',
                        choices = ep.MODEL_TYPES,
                        help    = 'pick type of electrical source model')
    group.add_argument('--propagation',
                        default = 'R-D',
                        choices = ep.PROP_TYPES,
                        help    =  'pick propagation driver, either R-D, R-E- '
                                   '(eikonal no diffusion) or R-E+ (eikonal with diffusion).')
    group.add_argument('--dx3D',
                        type    = float,
                        default = 200.,
                        help    = 'Discretization of volumentric elements (microns)')
    group.add_argument('--resist',
                        type    = float,
                        default = 100.,
                        help    = 'Resistance between Purkinje fibers (kOhm)')
    group.add_argument('--cond',
                        type    = float,
                        default = 0.0006,
                        help    = '')
    group.add_argument('--effcond',
                        type    = bool,
                        default = True,
                        help    = 'Scale purkinje conductivity based on segment length. '
                                  'Default: 1')
    group.add_argument('--dx1D',
                        type    = float,
                        default = 100.,
                        help    = 'Discretization of purkinje segments (microns)')
    group.add_argument('--tend',
                        type    = float,
                        default = 10.,
                        help    = 'Duration of simulation (ms)')
    return parser

class Purkinje(object):
    def __init__(self):
        # Rationalise and save constructor arguments
        self._parents  = []
        self._sons     = []
        self._cables   = []
        self._nlst     = []
        # network parameters
        self._radius   =     7
        self._resist   =   100
        self._cond     =     0.0006
        self._effcond  =     1
        # junctional parameters
        #self._nPMJ_min =   500
        #self._nPMJ_max =   600
        self._PMJsize  =   500
        self._PMJscale = 80000
        self._Rpmj     = 85000


    def add_cable(self, cables, nlst, parents=[], sons=[]):

        # check if node indices match with length of nlst:
        self._cables.append(cables)
        for node in nlst:
            self._nlst.append(node)

        self._parents.append(parents)
        self._sons.append(sons)
        # update parents
        # update sons

    def set_segment_lengths(self, dx):
        for cable in self._cables:
            nodeIndex = 0
            while nodeIndex<(len(cable)-1) or nodeIndex==0:
                idxA  = cable[nodeIndex  ]
                idxB  = cable[nodeIndex+1]
                nodeA = self._nlst[idxA]
                nodeB = self._nlst[idxB]
                d     = self.vec_mag(nodeA, nodeB)

                if d >= 2*dx:
                    # reduce segment length
                    vecAB = self.vec_sub(nodeA, nodeB)
                    nodeC = self.vec_add(nodeA, self.vec_scale(vecAB, 1/d*dx))

                    # add new point to purkinje structure
                    cable.insert(nodeIndex+1, len(self._nlst))
                    self._nlst.append(nodeC)
                    del idxA, idxB, nodeA, nodeB, nodeC, vecAB, d

                nodeIndex += 1

    def save(self, fname):
        print('Writing PS data to {}.pkje'.format(fname))

        num_cables = len(self._cables)

        # open filehandle
        with open(fname + ".pkje", "w") as fid:
            fid.write("{}\n".format(num_cables))   # cables

            for cIndx in range(num_cables):
                fid.write("Cable {}\n".format(cIndx))
                fid.write("{} {}\n".format(self._parents[cIndx][0], self._parents[cIndx][1]))
                fid.write("{} {}\n".format(self._sons   [cIndx][0], self._sons   [cIndx][1]))
                fid.write("{}\n".format(len(self._cables[cIndx])))

                fid.write("{}\n".format(self._radius))
                fid.write("{}\n".format(self._resist))
                fid.write("{}\n".format(self._cond  ))

                cable     = self._cables[cIndx]
                num_nodes = len(cable)

                for i in range(num_nodes):
                    nIndx = cable[i]
                    fid.write("{} {} {}\n".format(self._nlst[nIndx][0],
                                                  self._nlst[nIndx][1],
                                                  self._nlst[nIndx][2]))

    def vec_mag(self, a, b):
        return math.sqrt( (b[0]-a[0])**2 +
                          (b[1]-a[1])**2 +
                          (b[2]-a[2])**2 )

    def vec_scale(self, a, const):
        return [a[0]*const, a[1]*const, a[2]*const]

    def vec_add(self, a, b):
        return [a[0]+b[0],
                a[1]+b[1],
                a[2]+b[2]]

    def vec_sub(self, a, b):
        return [b[0]-a[0],
                b[1]-a[1],
                b[2]-a[2]]



def setupPurkNetwork(x, y, z, dx, meshname):
    # Units are microns
    purk = Purkinje()

    # add start of purkinje network
    purk.add_cable([0,1],
                   [[x/3., y, z], [x/3., y-5*dx, z]],
                   parents = [-1,-1],
                   sons    = [ 1, 2])

    purk.add_cable([1,2],
                   [[2*x/3., y-5*dx, z]],
                   parents = [ 0, -1],
                   sons    = [-1, -1])

    purk.add_cable([1,3],
                   [[x/3., 0, z]],
                   parents = [ 0, -1],
                   sons    = [-1, -1])

    purk.set_segment_lengths(dx)
    purk.save(meshname)

    return purk

def compute_cv_purkinje(simID):

    # read data from hexahedral grid
    block = mesh.Mesh()
    block.read_points(os.path.join(simID, 'block_i.pts'))

    # read mesh + purkinje network
    myopurk  = mesh.Mesh()
    myopurk.read_points(os.path.join(simID, 'myopurk.pts'))

    purk = myopurk.points()[block.num_pts:]

    # read activation file
    for fname in os.listdir(simID):
        if 'LAT' not in fname:
            continue
        with open(os.path.join(simID, fname)) as fp:
            lats = np.loadtxt(fp)
            break

    # Discard non-Purkinje LATs
    lats = lats[block.num_pts:]

    # determine two nodes to compute conduction velocity for
    bboxYmax = purk[:,1].max()
    bboxYmin = purk[:,1].min()

    # 'desired' locations for measuring CV
    ptA    = np.copy(purk[0])
    ptA[1] = bboxYmax - 0.5     # first site for measuring
    ptB    = np.copy(purk[0])
    ptB[1] = bboxYmin + 0.5     # second site for measuring

    # determine corresponding point indices
    dist = np.linalg.norm((purk - ptA), axis=1)
    idxA = dist.argmin()

    dist = np.linalg.norm((purk - ptB), axis=1)
    idxB = dist.argmin()

    # CV calculation
    # Calculate distance between actual points
    distAB  = np.linalg.norm(purk[idxA] - purk[idxB])
    t_act_A = lats[idxA]
    t_act_B = lats[idxB]

    if t_act_A >= 0 and t_act_B >= 0 and distAB > 0:
        cv = distAB / (t_act_B - t_act_A) # mm/ms = m/s
    else:
        cv = np.nan

    # save information in file
    with open(os.path.join(simID, "cv_purkinje.dat"), "w") as fp:
        fp.write("CV = {0:.3f} m/s\n".format(cv))

    print('  ---------------------------')
    print('  CV Purkinje: {0:.2f} m/s'.format(cv))
    print('    Node A {} [{}] activated @ {} ms'.format(purk[idxA], idxA, t_act_A))
    print('    Node B {} [{}] activated @ {} ms'.format(purk[idxB], idxB, t_act_B))
    print('    Distance {} mm'.format(distAB))

    return cv

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_{}_{}_np{}'
    return tpl.format(today.isoformat(), args.sourceModel, args.propagation,
                      args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Units are mm
    # Modify x and y to be a multiples of the mesh resolution
    # x -> 3x mesh resolution
    # y -> 10mm
    # z -> 1x mesh resolution (thin in z-direction)
    x    = args.dx3D/1000*3                                # in mm
    y    = math.ceil(10./args.dx3D*1000.)*args.dx3D/1000.  # in mm
    z    = args.dx3D/1000.
    geom = mesh.Block(size       = (x, y, z),
                      resolution = args.dx3D/1000.,
                      centre     = (x/2., y/2., z/2.))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Generate purkinje network
    # Units are microns
    purkfile = 'meshes/purkinje'
    purk = setupPurkNetwork(x*1000., y*1000., z*1000., args.dx1D, purkfile)
    # Update with user-input
    purk._radius   =     7
    purk._resist   = args.resist
    purk._cond     = args.cond
    purk._effcond  = int(args.effcond)
    # Other defaults
    #purk._nPMJ_min =    20
    #purk._nPMJ_max =   100
    purk._PMJ_size =   500 # radius/diameter ? in microns
    purk._PMJscale = 80000
    purk._Rpmj     = 85000


    # Get basic command line, including solver options
    cmd = tools.carp_cmd('carp.par')

    cmd += ['-simID',    job.ID,
            '-meshname', meshname,
            '-tend',     args.tend]

    # Add purkinje options
    # Junctional (PMJ) parameters
    cmd += [#'-nPMJ_min',     purk._nPMJ_min,
            #'-nPMJ_max',     purk._nPMJ_max,
            '-PMJsize',      purk._PMJsize,
            '-PMJscale',     purk._PMJscale,
            '-Rpmj',         purk._Rpmj]
    # Network (PS) parameters
    cmd += ['-purk_resist',  purk._resist,
            '-purk_cond',    purk._cond,
            '-purk_effcond', purk._effcond,]
    # CARP specific purkinje settings
    cmd += ['-numPurkIon',          1,
            '-PurkIon[0].im',       'ARPF',
            '-PurkIon[0].name',     'PS',
            '-PurkIon[0].im_param', "g_Na*1.0",
            '-purkf',               purkfile]

    cmd += ['-gridout_i', 3,
            '-gridout_e', 3]

    cmd += ['-phie_rec_ptf',       'meshes/ecg.pts',
            '-phie_recovery_file', 'ecg.igb']

    # Run simulation
    job.carp(cmd)

    # Compute conduction velocity
    # if not settings.cli.dry:
    #     cv_purk = compute_cv_purkinje(job.ID)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, 'myopurk')
        data = os.path.join(job.ID, 'vm.igb')
        view = 'view.mshz'

        # Call meshalyzer
        job.meshalyzer(geom, data, view)


# Define some tests
__tests__ = []
desc = ('Generate a HIS activation sequence within a thin cuboid and compare '
        ':math:`V_m` against reference.')
test = testing.Test('default', run,
                    ['--np', 2, '--flavor', 'petsc'],
                    description=desc,
                    tags=[testing.tag.FAST,
                    testing.tag.PARALLEL,
                    testing.tag.PURKINJE])
test.add_filecmp_check('vm.igb', testing.max_error, 0.001)
test.add_filecmp_check('ecg.igb', testing.max_error, 0.001)
__tests__.append(test)

# test = testing.Test('psnod', run,
#                     ['--np', 4, '--flavor', 'pt', '--CARP-opts', '-num_PS_nodes 2'],
#                     description=desc,
#                     tags=[testing.tag.FAST,
#                     testing.tag.PARALLEL,
#                     testing.tag.PURKINJE])
# test.add_filecmp_check('vm.igb', testing.max_error, 0.001)
# test.add_filecmp_check('ecg.igb', testing.max_error, 0.001)
# __tests__.append(test)



if __name__ == '__main__':
    run()
