#!/usr/bin/env python3

"""
This is a simple example to demonstrate the leadfield approach.

Problem Setup
=============

.. image:: /images/LeadField_Setup.png

Usage
=====

As with other examples, add the ``--visualize`` option to automatically load
the results in meshalyzer:

.. code-block:: bash

    ./run.py --tend 20 --sourceModel bidomain --visualize
"""
import os
from datetime import date

from carputils import settings
from carputils import tools
from carputils import testing
from carputils import mesh
from carputils import ep
from carputils.carpio import txt, igb

from matplotlib import pyplot as plt
import numpy as np

EXAMPLE_DESCRIPTIVE_NAME = 'LeadField'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>,' \
                 'Anton Prassl <anton.prassl@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)


def parser():
    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')
    group.add_argument('--sourceModel',
                        default='monodomain',
                        choices=ep.MODEL_TYPES,
                        help='pick type of electrical source model')
    group.add_argument('--propagation',
                        default='R-D',
                        choices=ep.PROP_TYPES,
                        help='pick propagation driver, either Reaction-Diffusion, '
                             'Reaction-Eikonal or prescribe AP shape using Eikonal'
                             ' as a trigger')
    group.add_argument('--lfmode',
                        default=0,
                        type=int,
                        choices=[0, 1],
                        help='1 ... solve as Dirichlet problem (reference node is grounded) \n'
                             '0 ... solve as pure Neumann problem (current of strength -1 withdrawn at reference node')
    group.add_argument('--tend',
                        type=float,
                        default=20,
                        help='Duration of simulation (ms)')
    return parser


# determine nearest neighbor by index
def closest_node(pt, pts):
    pts = np.asarray(pts)
    dist_2 = np.sum((pts - pt)**2, axis=1)
    return np.argmin(dist_2)


# build mesh with lower left at origin
def create_mesh():
    # generate mesh (units in mm)
    geom = mesh.Block(size=(5, 5, 2), etype='tetra', resolution=0.1)
    geom.corner_at_origin()
    geom.set_fibres(0, 0, 90, 90) # set fibre angle to 0, sheet angle to 0
    geom.set_bath(thickness=(0, 0, 1), both_sides=False)

    # generate and return base name
    meshname = mesh.generate(geom)
    return meshname, geom


# define spherical stimulus geometry at the origin
def query_stimIDs(args, meshname, STIMRADIUS):

    stimIDs = []
    numelems = 0
    numpts = 0
    if not args.dry:
        pts, numpts = txt.read(meshname + '.pts')
        elems, elemtags, numelems = txt.read(meshname + '.elem')
        sqrdist = np.zeros([numpts,3])
        for i in range(3):
            sqrdist[:,i] = pts[:,i]**2
        dist = np.sqrt(np.sum(sqrdist, axis=1))
        # now collect all vertex ids within certain radius
        stimIDs = np.where(dist < STIMRADIUS)
        stimIDs = stimIDs[0]
    return stimIDs, numelems, numpts


def get_LF_geom(geom, meshname):
    CM2UM = 1000

    bbox_bath = geom.bath_limits()
    ul_bath   = bbox_bath[1] * CM2UM
    pts, numpts = txt.read(meshname + '.pts')

    # define spatial locations LF nodes
    pt_LF0 = [0, 0, ul_bath[2]]  # serves as LF reference
    pt_LF1 = [ul_bath[0], 0, ul_bath[2]]
    pt_LF2 = [ul_bath[0], ul_bath[1], ul_bath[2]]
    pt_LF3 = [0, ul_bath[1], ul_bath[2]]

    # query node index for LF nodes
    vtx_LF0 = closest_node(pt_LF0, pts)  # LF reference
    vtx_LF1 = closest_node(pt_LF1, pts)
    vtx_LF2 = closest_node(pt_LF2, pts)
    vtx_LF3 = closest_node(pt_LF3, pts)

    return vtx_LF0, vtx_LF1, vtx_LF2, vtx_LF3, pt_LF0, pt_LF1, pt_LF2, pt_LF3


def compare_traces(t, LF1, LF2, LF3, phie0, phie1, phie2, phie3, args, simID):
    fs = 15
    scale_phie_rec = 90

    fig = plt.figure()
    fig.suptitle('{}-{}-mode{}'.format(args.sourceModel, args.propagation, args.lfmode))
    # ---I----------------------------------------------------------------------
    plt.subplot(411)
    #plt.plot(_t, LF0, 'k-', label='$LF0$')
    plt.plot(t, phie0, 'r-', label='$Ref$')
    plt.grid(True, color='k')
    #plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')
    # -------------------------------------------------------------------------
    plt.subplot(412)
    plt.plot(t, LF1, 'k.', label='$LF1$')
    if 'pseudo' in args.sourceModel:
        plt.plot(t-args.spacedt, phie1 - phie0, 'r-', label='$phi_1$-Ref (shifted by {}ms!)'.format(args.spacedt))
    elif args.sourceModel == 'monodomain':
        plt.plot(t, (phie1-phie0)*scale_phie_rec, 'r-', label='$phi_1$-Ref (scaled!)')
    else:
        plt.plot(t, phie1-phie0, 'r-', label='$phi_1$-Ref')
    plt.grid(True, color='k')
    #plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')
    # -------------------------------------------------------------------------
    plt.subplot(413)
    plt.plot(t, LF2, 'k.', label='$LF2$')
    if 'pseudo' in args.sourceModel:
        plt.plot(t-args.spacedt, phie2 - phie0, 'r-', label='$phi_2-Ref$ (shifted by {}ms!)'.format(args.spacedt))
    elif args.sourceModel == 'monodomain':
        plt.plot(t, (phie2-phie0)*scale_phie_rec, 'r-', label='$phi_2$-Ref (scaled!)')
    else:
        plt.plot(t, phie2-phie0, 'r-', label='$phi_2-Ref$')
    plt.grid(True, color='k')
    #plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')
    # -------------------------------------------------------------------------
    plt.subplot(414)
    plt.plot(t, LF3, 'k.', label='$LF3$')
    if 'pseudo' in args.sourceModel:
        plt.plot(t-args.spacedt, phie3 - phie0, 'r-', label='$phi_3-Ref$ (shifted by {}ms!)'.format(args.spacedt))
    elif args.sourceModel == 'monodomain':
        plt.plot(t, (phie3-phie0)*scale_phie_rec, 'r-', label='$phi_3$-Ref (scaled!)')
    else:
        plt.plot(t, phie3-phie0, 'r-', label='$phi_3-Ref$')
    plt.grid(True, color='k')
    plt.xlabel(r'time (ms)', fontsize=fs)
    plt.ylabel(r'U (mV)')
    plt.legend(loc='best')
    # -------------------------------------------------------------------------
    plt.show()
    fig.savefig(os.path.join(simID, 'lf_comparison.eps'), format='eps', papertype='a4', transparent=True)
    return


def update_cmd(cmd, key, value):
    length = len(cmd)
    for index in range(length):
        if not isinstance(cmd[index], str):
            continue
        if key == cmd[index]:
            cmd[index + 1] = value
            break
    return cmd


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_LF_{}_{}_{}_lfmode{}_np{}'.format(today.isoformat(), args.propagation, args.sourceModel, args.flv, args.lfmode, args.np)


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-\d{2}-\d{2})|(mesh)|(LF)')
def run(args, job):
    EMPHASIZE = '\x1b[0;30;43m{}\x1b[0m'
    STIMRADIUS = 250
    stimIDx = 0
    args.spacedt = 1

    if os.path.exists(os.path.join(EXAMPLE_DIR, 'LF')):
        job.rm(os.path.join(EXAMPLE_DIR, 'LF'), rmdir=True)

    # create mesh
    meshname, geom = create_mesh()
    meshdir = os.path.dirname(meshname)


    # --- PRE-DEFINED FILE NAMES ----------------------------------------------
    # lead field directory
    lf_dir = os.path.abspath(os.path.join(EXAMPLE_DIR, 'LF'))
    # vertex filename containing the stimulus nodes
    stim_vtxfile = os.path.join(meshdir, 'stim0')
    # vertex filename containing the lead field source nodes
    lf_src_vtxfile = os.path.join(meshdir, 'lf_sources')
    # vertex file containing the lead field reference node
    lf_ref_vtxfile = os.path.join(meshdir, 'lf_ref')
    # data file for outputting lead field based electrograms
    lf_egm_datfile = 'lfEGMs'
    # points file containing leadfield reference and source nodes
    phie_recv_ptfile = os.path.join(meshdir, 'phie_recv_ptfile')
    # -------------------------------------------------------------------------


    lf_ref_vtx = -1
    # define spherical stimulus geometry at the origin
    if os.path.exists(meshname + '.pts') and not args.dry:
        stimIDs, numelems, numpts = query_stimIDs(args, meshname, STIMRADIUS)

        # export spherical stimulus as vtx file
        txt.write(stim_vtxfile + '.vtx', stimIDs, domain='extra')

        # asemble leadfield source locations
        vtx_LF0, vtx_LF1, vtx_LF2, vtx_LF3, pt_LF0, pt_LF1, pt_LF2, pt_LF3 = get_LF_geom(geom, meshname)
        lf_src_vtx = [vtx_LF1, vtx_LF2, vtx_LF3]
        lf_ref_vtx = vtx_LF0
        num_lf_src = len(lf_src_vtx)

        # export leadfield source locations
        txt.write(lf_src_vtxfile + '.vtx', (lf_src_vtx), domain='extra')
        # export LFref to mesh  directory (not used file!)
        txt.write(lf_ref_vtxfile + '.vtx', [lf_ref_vtx], domain='extra')

        # export all leadfield nodes for phie recovery
        txt.write(phie_recv_ptfile + '.pts', np.array([pt_LF0, pt_LF1, pt_LF2, pt_LF3]))


    # -------------------------------------------------------------------------
    # ---PRE-COMPUTE THE LEADFIELD MATRIX--------------------------------------
    # -------------------------------------------------------------------------
    # get basic command line, including solver options
    stdcmd = []
    stdcmd += ['-meshname', meshname]
    stdcmd += ['-lead_field_ref',     lf_ref_vtx]
    stdcmd += ['-lead_field_sources', lf_src_vtxfile]
    stdcmd += ['-lead_field_mode',  args.lfmode]  # mode 0 current balancing at ref node
                                                  # mode 1 grounding of ref node
    stdcmd += ['-lead_field_meth',  1]
    stdcmd += ['-dump_lead_fields', 1]

    if not os.path.exists(lf_dir):
        print(EMPHASIZE.format('Computing lead field matrix...'))
        cmd = tools.carp_cmd('carp.par')
        cmd += ['-simID',   lf_dir]
        cmd += ['-bidomain',     1]
        cmd += ['-experiment',   9]
        cmd += stdcmd

        # run carp simulation
        job.carp(cmd)
    else:
        print(EMPHASIZE.format('Found lead field directory. Skipping generation...'))
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------


    # setup next CARP run
    cmd = tools.carp_cmd('carp.par')
    cmd += ep.model_type_opts(args.sourceModel)
    cmd += ['-simID',               job.ID]
    cmd += ['-num_stim',            stimIDx+1]
    cmd += ['-dt',                  25]
    cmd += ['-spacedt',             args.spacedt]
    cmd += ['-timedt',              1]
    cmd += ['-tend',                args.tend]
    cmd += ['-lead_field_egm_file', lf_egm_datfile]     # output parameter
    cmd += ['-lead_field_dir',      lf_dir]
    if args.visualize:
        cmd += ['-gridout_i',       3]
        cmd += ['-gridout_e',       3]

    # add stimulus nodes
    cmd += ['-stimulus[{}].vtx_file'.format(stimIDx), stim_vtxfile]

    # add GND for bidomain mode
    if args.sourceModel == 'bidomain':
        stimIDx += 1
        # leadfield reference must be grounded in bidomain mode
        cmd += ['-stimulus[{}].name'.format(stimIDx), 'LF_REF_GND']
        cmd += ['-stimulus[{}].stimtype'.format(stimIDx), 3]
        cmd += ['-stimulus[{}].vtx_file'.format(stimIDx), lf_ref_vtxfile]
        update_cmd(cmd, '-num_stim', stimIDx + 1)

    # setup eikonal
    if 'R-E' in args.propagation:
        cmd += ['-num_ekregions',      1]
        cmd += ['-ekregion[0].name',   'TISSUE']
        cmd += ['-ekregion[0].ID',     0]
        cmd += ['-ekregion[0].vel_f',  0.45]
        cmd += ['-ekregion[0].vel_s',  0.28]
        cmd += ['-ekregion[0].vel_n',  0.28]

        stimIDx += 1
        cmd += ['-stimulus[{}].name'.format(stimIDx), 'eikonal'.upper()]
        cmd += ['-stimulus[{}].stimtype'.format(stimIDx), 8]
        update_cmd(cmd, '-num_stim', stimIDx+1)
        update_cmd(cmd, '-dt', 100)

    # recover phie's in monodomain mode
    if args.sourceModel == 'monodomain':
        cmd += ['-phie_rec_ptf', phie_recv_ptfile]
        cmd += ['-phie_rec_meth', 1]
        cmd += ['-phie_recovery_file', 'phie_recv']

    # Run simulation
    cmd += stdcmd
    job.carp(cmd)

    # do visualization
    if args.visualize and not settings.platform.BATCH:

        # read leadfield EGMs
        lfEGM = txt.read(os.path.join(job.ID, lf_egm_datfile + '.dat'))
        lfEGM = lfEGM.reshape((int(len(lfEGM) / num_lf_src), num_lf_src))
        data_LF1 = lfEGM[:, 0]
        data_LF2 = lfEGM[:, 1]
        data_LF3 = lfEGM[:, 2]

        # Prepare file paths
        if args.sourceModel == "monodomain":
            geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
            data = os.path.join(job.ID, 'vm.igb')
            view = os.path.join(EXAMPLE_DIR, 'view_vm.mshz')
        else:
            geom = os.path.join(job.ID, os.path.basename(meshname)+'_e')
            data = os.path.join(job.ID, 'phie.igb')
            view = os.path.join(EXAMPLE_DIR, 'view_phie.mshz')

        # call meshalyzer
        job.meshalyzer(geom, data, view)


        # Compare leadfield EGMs with bidomain, if previously computed
        if not args.dry:
            # read phie traces
            if 'bidomain' in args.sourceModel:
                phie, header, t = igb.read(os.path.join(job.ID, 'phie.igb'))
                phie_0 = phie[vtx_LF0, :]
                phie_1 = phie[vtx_LF1, :]
                phie_2 = phie[vtx_LF2, :]
                phie_3 = phie[vtx_LF3, :]
            else:
                # read recovered phie file
                phie, header, t = igb.read(os.path.join(job.ID, 'phie_recv.igb'))
                phie_0 = phie[0, :]
                phie_1 = phie[1, :]
                phie_2 = phie[2, :]
                phie_3 = phie[3, :]

            # plot comparison
            compare_traces(t, data_LF1, data_LF2, data_LF3,
                           phie_0, phie_1, phie_2, phie_3,
                           args, job.ID)


# Define some tests
__tests__ = []

# -----------------------------------------------------------------------------
# ---PARALLEL------------------------------------------------------------------
# -----------------------------------------------------------------------------
# bidomain - pt - R-D
test = testing.Test('parallel_bd_RD_pt', run,
                    ['--sourceModel', 'bidomain', '--flavor', 'pt', '--propagation', 'R-D', '--np', 8],
                    tags = [testing.tag.MEDIUM,
                            testing.tag.BIDOMAIN,
                            testing.tag.PARALLEL])
test.add_filecmp_check('phie.igb', testing.max_error, 0.001)
test.add_filecmp_check('lfEGMs.dat', testing.max_error, 0.001)
__tests__.append(test)

# bidomain - petsc - R-D
test = testing.Test('parallel_bd_RD_petsc', run,
                    ['--sourceModel', 'bidomain', '--flavor', 'petsc', '--propagation', 'R-D', '--np', 8],
                    refdir = 'parallel_bd_RD_pt',
                    tags = [testing.tag.MEDIUM,
                            testing.tag.BIDOMAIN,
                            testing.tag.PARALLEL])
test.add_filecmp_check('phie.igb', testing.max_error, 0.001)
test.add_filecmp_check('lfEGMs.dat', testing.max_error, 0.001)
test.disable_reference_generation()
__tests__.append(test)

# pseudo-bidomain - petsc - R-E+
test = testing.Test('parallel_pbd_RE_petsc', run,
                    ['--sourceModel', 'pseudo_bidomain', '--flavor', 'petsc', '--propagation', 'R-E+', '--np', 8],
                    tags = [testing.tag.MEDIUM,
                            testing.tag.PSEUDO_BIDOMAIN,
                            testing.tag.EIKONAL,
                            testing.tag.PARALLEL])
test.add_filecmp_check('phie.igb', testing.max_error, 0.001)
test.add_filecmp_check('lfEGMs.dat', testing.max_error, 0.001)
__tests__.append(test)

# pseudo-bidomain - pt - R-E+
test = testing.Test('parallel_pbd_RE_pt', run,
                    ['--sourceModel', 'pseudo_bidomain', '--flavor', 'pt', '--propagation', 'R-E+', '--np', 8],
                    refdir = 'parallel_pbd_RE_petsc',
                    tags = [testing.tag.MEDIUM,
                            testing.tag.PSEUDO_BIDOMAIN,
                            testing.tag.EIKONAL,
                            testing.tag.PARALLEL])
test.add_filecmp_check('phie.igb', testing.max_error, 0.001)
test.add_filecmp_check('lfEGMs.dat', testing.max_error, 0.001)
test.disable_reference_generation()
__tests__.append(test)

# monodomain - pt - R-E+
test = testing.Test('parallel_md_RE_pt', run,
                    ['--sourceModel', 'monodomain', '--flavor', 'pt', '--propagation', 'R-E+', '--np', 8],
                    tags = [testing.tag.MEDIUM,
                            testing.tag.MONODOMAIN,
                            testing.tag.EIKONAL,
                            testing.tag.PARALLEL])
test.add_filecmp_check('lfEGMs.dat', testing.max_error, 0.001)
test.add_filecmp_check('phie_recv.igb', testing.max_error, 0.001)
__tests__.append(test)
# -----------------------------------------------------------------------------


if __name__ == '__main__':
    run()
