#!/usr/bin/env python3

"""
Description TBD.
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Bidomain without a ground-type stimulus'
EXAMPLE_AUTHOR = 'Aurel Neic <aurel.neic@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')
    group.add_argument('--mode',
                        choices=['regular', 'prescribed', 'prescribed_file'],
                        default='regular',
                        help='choose if the field is updated regularly in bidomain mode, or using the prescribed-phie mode')
    group.add_argument('--tend',
                        type=float, default=15,
                        help='simulated time (ms)')
    group.add_argument('--phie_restr',
                        type=str, default='',
                        help='path to field sources restriction file')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_{}_{}_np{}'.format(today.isoformat(), args.flv, args.mode, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Add all the non-general arguments
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'sim.par'))

    if args.mode == 'regular':
      cmd += ['+F', 'stim.par']
    elif args.mode == 'prescribed_file':
      cmd += ['+F', 'stim_pp_file.par']
    else:
      cmd += ['+F', 'stim_pp.par']

    cmd += ['+F', 'tissue.par']

    if len(args.phie_restr):
      cmd += ['-phie_src_vtx', args.phie_restr]

    cmd += ['-simID',    job.ID,
            '-tend', args.tend]

    # Execute simulation
    job.carp(cmd)

# test settings
test_regular = testing.Test('regular', run, ['--mode','regular', '--np','4' ],
                             tags=[testing.tag.SHORT,
                                   testing.tag.BIDOMAIN,
                                   testing.tag.PARALLEL])
test_regular.add_filecmp_check('phie_i.igb.gz', testing.max_error, 0.001)
test_regular.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)
test_regular.add_filecmp_check('ps_vm.igb.gz', testing.max_error, 0.001)

test_prescribed = testing.Test('prescribed', run, ['--mode','prescribed', '--np','4' ],
                             tags=[testing.tag.SHORT, testing.tag.PARALLEL])
test_prescribed.add_filecmp_check('phie_i.igb.gz', testing.max_error, 0.001)
test_prescribed.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)
test_prescribed.add_filecmp_check('ps_vm.igb.gz', testing.max_error, 0.001)


__tests__ = [test_regular, test_prescribed]
#__tests__ = [test_regular]

if __name__ == '__main__':
    run()
