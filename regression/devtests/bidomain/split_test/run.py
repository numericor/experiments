#!/usr/bin/env python3

"""
"""
import os

EXAMPLE_DESCRIPTIVE_NAME = 'Splitting tst'
EXAMPLE_AUTHOR = 'Aurel Neic'
EXAMPLE_DIR = os.path.dirname(__file__)

from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_split_{}_np{}'.format(today.isoformat(), args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Get basic command line, including solver options
    cmd = tools.carp_cmd(os.path.join(EXAMPLE_DIR, 'sim.par'))
    cmd += tools.gen_physics_opts(IntraTags=[1,2,3,4])
    meshname = 'mesh/block'

    cmd += ['-simID',    job.ID,
            '-meshname', meshname,
            '-tend',     50]

    if args.visualize:
        cmd += ['-gridout_i', 3]
        cmd += ['-gridout_e', 3]

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb.gz')
        view = os.path.join(EXAMPLE_DIR, 'simple.mshz')

        # Call meshalyzer
        job.meshalyzer(geom, data, view)

# Define some tests
desc = ('Generate activation sequence to test decoupling')
test_default = testing.Test('default', run, ['--np', '2'], description=desc,
                            tags=[testing.tag.FAST, testing.tag.PARALLEL])
test_default.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)

__tests__ = [test_default]

if __name__ == '__main__':
    run()
