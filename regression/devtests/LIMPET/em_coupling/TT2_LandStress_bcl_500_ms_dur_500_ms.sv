-85.8873            # Vm
1                   # Lambda
0                   # delLambda
0.000225708         # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.0014652           # Iion
-                   # tension_component
TT2
0.0807757           # Ca_i
1.46341             # CaSR
0.000363027         # CaSS
0.91218             # R_
1.69947e-07         # O
7.65164             # Na_i
138.265             # K_i
0.00151966          # M
0.760195            # H
0.696629            # J
0.0152906           # Xr1
0.477098            # Xr2
0.0185551           # Xs
2.1976e-08          # R
0.999996            # S
3.12541e-05         # D
0.716713            # F
0.948512            # F2
0.993657            # FCaSS

LandStress
0                   # Q_1
0                   # Q_2
0.00718998          # TRPN
1.58685e-06         # xb
0                   # __Ca_i_local

