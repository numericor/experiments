#!/usr/bin/env python

"""
This example tests the combination between various EP models with active tension
generating plugins in a single cell stretch test.

Usage
=====

Following optional arguments with their default values are available:

.. code-block:: bash

    ./run.py --model=MBRDR
    ./run.py --list
"""
import os, sys

EXAMPLE_DESCRIPTIVE_NAME = 'Electromechanical Single Cell Stretcher'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)

import numpy as np
from datetime import date
from matplotlib import pyplot

from carputils import settings
from carputils import bench
from carputils import testing
from carputils import tools

MODEL_EXCLUDE_LIST = ['IION_SRC', 'UCLA_RAB_SCR']

def parser():

    parser = tools.standard_parser()

    parser.add_argument('--experiment', default='active', 
                        choices=['active','passive'], 
                        help='pick experiment type (default is active)')
    parser.add_argument('--EP',  default='TT2', 
                        choices=['TT2','ORd','GPB','GPVatria','COURTEMANCHE'], 
                        help='pick human EP model (default is TT2)')
    parser.add_argument('--Stress', default='LandStress', 
                        choices=['LandStress','FxStress','TanhStress'], 
                        help='pick stress model (default is LandStress)')
#    parser.add_argument('--Coupling', default='weak', choices=['weak','strong'], help='coupling between EP and Stress model')
#    parser.add_argument('--SAC', default='NoSAC', choices=['NoSAC','SAC_KS','YHuSAC'], help='pick SAC model')
    parser.add_argument('--init',  default='', 
                        help='pick state variable initialization file (default is none)')
    parser.add_argument('--duration', default='500', 
                        help='pick duration of experiment (default is 500 ms)')
    parser.add_argument('--bcl', default='500', 
                        help='pick basic cycle length (default is 500 ms)')
    parser.add_argument('--fs', default='', 
                        help='provide trace file to prescribe fiber stretch (default is none)')
    parser.add_argument('--fsDot', default='', 
                        help='provide trace file to prescribe fiber stretch rate (default is none)')
    parser.add_argument('--refH5',  default='', 
                         help='pick h5 data set as reference for comparison (default is none)')

    return parser
    
def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_bench_{}_{}_np{}'
    return tpl.format(today.isoformat(), args.EP, args.Stress, args.np)
   
# prescribe single cell stretch protocol
def prescribe_stretch(args):

    
    if args.fs != '':

        if not os.path.isfile(args.fs):
          print('Fiber stretch file %s not found!'%(args.fs))
          sys.exit(-1)
    
        load_sv = 'Lambda'
        load_sv_file = '%s'%(args.fs)
        if(args.fsDot!=''):
          if not os.path.isfile(args.fsDot):
            print('Fiber stretch rate file %s not found!'%(args.fsDot))
            sys.exit(-1)
    
          load_sv += ':delLambda'
          load_sv_file += ':%s'%args.fsDot
    
        extLoad = [ '--clamp-SVs', load_sv,
                     '--SV-clamp-files', load_sv_file ]

    else:
        # no fiber stretch prescribed, keep cell isometric
        extLoad = []

    return extLoad


def EP_active_tension_params(args):

    # for each combination between EP and Stress model assign appropriate settings
    stressPars = []
    if args.EP == 'TT2' and args.Stress == 'LandStress':
        stressPars  = ['--plug-par']
        stressPars += ['T_ref=117.1,Ca_50ref=0.72,TRPN_50=0.37,n_TRPN=1.54,k_TRPN=0.14,n_xb=3.38,k_xb=4.9e-3' ]

    else:
        print('No preset values available for combination {} and {}'.format(args.EP,args.Stress))

    return stressPars


def launchLimpetGUI(args,job):

    # build hdf5 file
    tracesHeader = '{}/{}_{}_header.txt'.format(job.ID,args.EP,args.Stress)
    traceH5      = '{}_{}_{}_traces.h5'.format(job.ID,args.EP,args.Stress)
    h5cmd        = [ settings.execs.sv2h5b, tracesHeader, traceH5 ]
   
    job.bash(h5cmd)


    # limpetGUI command
    vcmd  = [settings.execs.limpetgui, '{}_{}_{}_traces.h5'.format(job.ID,args.EP,args.Stress)]

    # GUI adjustments, only for qwt version
    GUIsettings = [ '--vspacing=20', 
                    '--hspacing=20' ]

    #vcmd += GUIsettings

    # do we have a reference trace to compare against?
    if args.refH5 != '':
      vcmd += [args.refH5]

    job.bash(vcmd)


@tools.carpexample(parser, jobID)
def run(args, job):

    # run bench with available ionic models
    cmd  = [settings.execs.BENCH,
            '--imp={}'.format(args.EP), 
            '--plug-in={}'.format(args.Stress),
            '--duration', args.duration ]

    # apply stimulus current or observe only response to imposed stretch?
    numstim = 0
    if args.experiment == 'passive':
        cmd += [ '--stim-curr',       0.0,
                 '--numstim',         0,
                 '--strain-rate',   150.,
                 '--strain',          0.2,
                 '--strain-time',    50.,
                 '--strain-dur',     20. ]

    else:
        cmd += [ '--stim-curr',      30.0,
                 '--numstim',        int(float(args.duration)/float(args.bcl)+1),
                 '--bcl',            args.bcl ]        

    # prescribe external loading protocols
    cmd += prescribe_stretch(args)


    # parametrize EP-active stress combination
    cmd += EP_active_tension_params(args)

    # numerical settings
    cmd += [ '--dt',            10.0e-3 ]

    # IO and state management
    cmd += [ '--dt-out',        0.1,
             '--save-ini-file', '{}_{}_bcl_{}_ms_dur_{}_ms.sv'.format(args.EP,args.Stress,args.bcl,args.duration),
             '--save-ini-time', args.duration ]

    # use steady state initialization vectors
    if args.init!='':
        if not os.path.isfile(args.init):
            print('State variable initialization file %s not found!'%(args.init))
            sys.exit(-1)
 
        cmd += ["--read-ini-file", args.init]


    if args.visualize and not settings.platform.BATCH:
        cmd += [ '--validate' ]


    # Output options
    cmd += ['--fout={}_{}'.format(os.path.join(job.ID, args.EP),args.Stress),
            '--bin' ]

    job.mpi(cmd, 'Testing {}-{}'.format(args.EP,args.Stress))

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        
        # simple visualization first, load and show Vm data
        dt  = np.dtype('<d') # little-endian
        tpl = os.path.join(job.ID, '{}_{}.{{}}.bin'.format(args.EP,args.Stress))
        t   = np.fromfile(tpl.format('t'),  dtype=dt)
        Vm  = np.fromfile(tpl.format('Vm'), dtype=dt)

        # Plot data
        pyplot.plot(t, Vm)
        pyplot.xlabel('Time (ms)')
        pyplot.ylabel('Transmembrane voltage (mV)')
        pyplot.show()

        # detailed visualization of all relevant traces
        launchLimpetGUI(args,job)


# Define some tests

# Get list of ionic models
# settings.cli = tools.standard_parser().parse_args(['--silent'])
# imps = bench.imps()
# 
# __tests__ = []
# 
# for imp in imps:
# 
#    if imp in MODEL_EXCLUDE_LIST:
#        continue
# 
#    desc = ('Compute :math:`V_m` trace for {} model and compare '
#            'against reference.'.format(imp))
#    
#    test = testing.Test(imp, run, 
#                        ['--model', imp],
#                        description=desc,
#                        tags=[testing.tag.FAST, testing.tag.SERIAL])
# 
#    test.add_filecmp_check('{}.Vm.bin'.format(imp), testing.max_error, 0.001)
# 
#    __tests__.append(test)
# 
if __name__ == '__main__':
    run()
