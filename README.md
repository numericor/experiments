Example experiments for openCARP based on the carputils framework.

For more details, see:
* [Rendered examples on webpage](https://opencarp.org/documentation/examples)
* [carputils repository](https://git.opencarp.org/openCARP/carputils)
* [openCARP respository](https://git.opencarp.org/openCARP/openCARP)
